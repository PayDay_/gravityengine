﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;

namespace GravityEngine
{
    static class Renderer
    {
        private static Shader shader; 

        private static int boundTexture;
        internal static int BoundTexture { get { return BoundTexture; } set { if (boundTexture != value) { GL.BindTexture(TextureTarget.Texture2D, value); boundTexture = value; } } }

		private static float GLSLVersion;
		private static bool NoInstancing;

        private static int vao;

        public static Matrix4 Projection;
        public static Matrix4 View = Matrix4.Identity;

        public static float MaxAnisotropySupported = 0;

        public static void Init() 
        {
            GLSLVersion = 130;

            Console.WriteLine("Renderer: " + GL.GetString(StringName.Renderer));
            Console.WriteLine("Vendor: " + GL.GetString(StringName.Vendor));
            Console.WriteLine("Version: " + GL.GetString(StringName.Version));
            //Console.WriteLine(float.Parse(GL.GetString(StringName.ShadingLanguageVersion)));
			string glslVersionString = GL.GetString(StringName.ShadingLanguageVersion);
            if (glslVersionString.Length > 6)
                glslVersionString = glslVersionString.Substring(0, glslVersionString.IndexOf(' '));

            float.TryParse(glslVersionString, out GLSLVersion);
			Console.WriteLine("GLSL Version: " + GLSLVersion);

			NoInstancing = GLSLVersion < 130f ? true : false;

            GL.DepthMask(true);
            GL.DepthFunc(DepthFunction.Lequal);
            GL.Enable(EnableCap.DepthTest);

            GL.Enable(EnableCap.CullFace);

            GL.BlendFunc(BlendingFactor.SrcAlpha, BlendingFactor.OneMinusSrcAlpha);

            MaxAnisotropySupported = GL.GetFloat((GetPName)All.MaxTextureMaxAnisotropyExt);

            MeshRenderer.Init();

            if (NoInstancing)
				shader = new Shader ("sprite_ni.vert", "sprite_ni.frag", true);
			else
            	shader = new Shader("sprite.vert", "sprite.frag", true);

            GL.UseProgram(shader.ProgramID);

            CreateVBO();

            FontRenderer.Init();
        }

        private static void CreateVBO()
        {
            List<Vector2> verts = new List<Vector2>();
            List<Vector2> texcoords = new List<Vector2>();

            verts.Add(new Vector2(1.0F, 1.0F));
            verts.Add(new Vector2(0.0F, 1.0F));
            verts.Add(new Vector2(1.0F, 0.0F));

            verts.Add(new Vector2(0.0F, 1.0F));
            verts.Add(new Vector2(0.0F, 0.0F));
            verts.Add(new Vector2(1.0F, 0.0F));

            texcoords.Add(new Vector2(1, 1));
            texcoords.Add(new Vector2(0, 1));
            texcoords.Add(new Vector2(1, 0));

            texcoords.Add(new Vector2(0, 1));
            texcoords.Add(new Vector2(0, 0));
            texcoords.Add(new Vector2(1, 0));

            Vector2[] vertdata = verts.ToArray();
            Vector2[] texcoorddata = texcoords.ToArray();

            vao = GL.GenVertexArray();
            GL.BindVertexArray(vao);
            shader.EnableVertexAttribArrays();

            GL.BindBuffer(BufferTarget.ArrayBuffer, shader.GetBuffer("position"));
			GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(vertdata.Length * Vector2.SizeInBytes), vertdata, BufferUsageHint.StaticDraw);
            GL.VertexAttribPointer(shader.GetAttribute("position"), 2, VertexAttribPointerType.Float, false, 0, 0);

            GL.BindBuffer(BufferTarget.ArrayBuffer, shader.GetBuffer("texCoord"));
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(texcoorddata.Length * Vector2.SizeInBytes), texcoorddata, BufferUsageHint.StaticDraw);
            GL.VertexAttribPointer(shader.GetAttribute("texCoord"), 2, VertexAttribPointerType.Float, true, 0, 0);

            if (!NoInstancing) // If Instancing...
			{
				int transformAttribute = shader.GetAttribute ("transform1");
				GL.BindBuffer (BufferTarget.ArrayBuffer, shader.GetBuffer ("transform1"));
				GL.VertexAttribPointer (transformAttribute, 4, VertexAttribPointerType.Float, false, 4 * Vector4.SizeInBytes, (IntPtr)0);
				GL.VertexAttribPointer (transformAttribute + 1, 4, VertexAttribPointerType.Float, false, 4 * Vector4.SizeInBytes, (IntPtr)(1 * Vector4.SizeInBytes));
				GL.VertexAttribPointer (transformAttribute + 2, 4, VertexAttribPointerType.Float, false, 4 * Vector4.SizeInBytes, (IntPtr)(2 * Vector4.SizeInBytes));
				GL.VertexAttribPointer (transformAttribute + 3, 4, VertexAttribPointerType.Float, false, 4 * Vector4.SizeInBytes, (IntPtr)(3 * Vector4.SizeInBytes));
				GL.VertexAttribDivisor (transformAttribute, 1);
				GL.VertexAttribDivisor (transformAttribute + 1, 1);
				GL.VertexAttribDivisor (transformAttribute + 2, 1);
				GL.VertexAttribDivisor (transformAttribute + 3, 1);

				GL.BindBuffer (BufferTarget.ArrayBuffer, shader.GetBuffer ("atlasRectangle"));
				GL.VertexAttribPointer (shader.GetAttribute ("atlasRectangle"), 4, VertexAttribPointerType.Float, false, 0, 0);
				GL.VertexAttribDivisor (shader.GetAttribute ("atlasRectangle"), 1);

				GL.BindBuffer (BufferTarget.ArrayBuffer, shader.GetBuffer ("color"));
				GL.VertexAttribPointer (shader.GetAttribute ("color"), 4, VertexAttribPointerType.Float, false, 0, 0);
				GL.VertexAttribDivisor (shader.GetAttribute ("color"), 1);
			}

            shader.DisableVertexAttribArrays();
        }

        public static void Render()
        {
            GravityWindow.DrawCalls = 0;
            GL.Clear(ClearBufferMask.ColorBufferBit);
            GL.Clear(ClearBufferMask.DepthBufferBit);

            if (Debug.Wireframe)
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
            else
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);

            if (Camera.ActiveCamera != null)
            {
                View = Camera.ActiveCamera.ViewMatrix;
                Projection = Camera.ActiveCamera.ProjectionMatrix;
            }
            MeshRenderer.Render();

            GL.Enable(EnableCap.Blend);

            GL.BindVertexArray(vao);
            GL.UseProgram(shader.ProgramID);
            shader.EnableVertexAttribArrays();

            if (Debug.Wireframe)
                GL.Uniform1(shader.GetUniform("wireframe"), 1);
            else
                GL.Uniform1(shader.GetUniform("wireframe"), 0);

			if (NoInstancing)
				RenderSingle();
			else
				RenderInstanced();

            shader.DisableVertexAttribArrays();

            FontRenderer.Render();

            GravityWindow.Window.SwapBuffers();
        }

		private static void RenderSingle()
		{
			foreach(SpriteRenderer sprite in SpriteRenderer.SpriteRenderers)
			{
                if (!sprite.Enabled)
                    continue;

                GL.Uniform1(shader.GetUniform("textured"), 1); //Tell shader to use texture
				GL.UniformMatrix4(shader.GetUniform("transform"), false, ref sprite.ModelMatrix);
				GL.Uniform4(shader.GetUniform("color"), sprite.Color);
				GL.Uniform4(shader.GetUniform("atlasRectangle"), sprite.AtlasRectangleTextureCoordinates);

				BoundTexture = sprite.Texture.ID;

				GL.DrawArrays(PrimitiveType.Quads, 0, 4);
                GravityWindow.DrawCalls++;
			}
		}

		private static void RenderInstanced()
		{
            GL.Uniform1(shader.GetUniform("textured"), 1); //Tell shader to use texture

            List<Matrix4> transformList = new List<Matrix4>();
            List<Vector4> atlasRectangleList = new List<Vector4>();
            List<Vector4> colorList = new List<Vector4>();

            Matrix4[] transforms;
            Vector4[] atlasRectangles;
            Vector4[] colors;

            #region World-Entities
            foreach (Texture texture in Texture.Textures)
			{
                transformList.Clear();
                atlasRectangleList.Clear();
                colorList.Clear();

                GL.Uniform4(shader.GetUniform("transparencyKey"), texture.TransparencyKey);

                BoundTexture = texture.ID;

				for (int i = 0; i < texture.Sprites.Count; i++)
				{
                    if (texture.Sprites[i].Entity.Transform.Space != Space.World)
                        continue;

					SpriteRenderer sprite = texture.Sprites[i];

                    if (!sprite.Enabled)
                        continue;

					transformList.Add(sprite.ModelMatrix);
					atlasRectangleList.Add(sprite.AtlasRectangleTextureCoordinates);
					colorList.Add(sprite.Color);
				}

                if (transformList.Count > 0)
                {
                    transforms = transformList.ToArray();
                    atlasRectangles = atlasRectangleList.ToArray();
                    colors = colorList.ToArray();

                    GL.BindBuffer(BufferTarget.ArrayBuffer, shader.GetBuffer("transform1"));
                    GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(Vector4.SizeInBytes * 4 * transforms.Length), transforms, BufferUsageHint.DynamicDraw);

                    GL.BindBuffer(BufferTarget.ArrayBuffer, shader.GetBuffer("atlasRectangle"));
                    GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(Vector4.SizeInBytes * atlasRectangles.Length), atlasRectangles, BufferUsageHint.DynamicDraw);

                    GL.BindBuffer(BufferTarget.ArrayBuffer, shader.GetBuffer("color"));
                    GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(Vector4.SizeInBytes * colors.Length), colors, BufferUsageHint.DynamicDraw);

                    GL.UniformMatrix4(shader.GetUniform("view"), false, ref View);
                    GL.UniformMatrix4(shader.GetUniform("projection"), false, ref Projection);

                    GL.DrawArraysInstanced(PrimitiveType.Quads, 0, 4, transforms.Length);
                    GravityWindow.DrawCalls++;
                }
                
            }
            #endregion

            GL.Clear(ClearBufferMask.DepthBufferBit);

            #region GUI-Entities
            foreach (Texture texture in Texture.Textures)
            {
                transformList.Clear();
                atlasRectangleList.Clear();
                colorList.Clear();

                BoundTexture = texture.ID;

                for (int i = 0; i < texture.Sprites.Count; i++)
                {
                    if (texture.Sprites[i].Entity.Transform.Space != Space.Screen)
                        continue;

                    SpriteRenderer sprite = texture.Sprites[i];

                    if (!sprite.Enabled)
                        continue;

                    transformList.Add(sprite.ModelMatrix);
                    atlasRectangleList.Add(sprite.AtlasRectangleTextureCoordinates);
                    colorList.Add(sprite.Color);
                }

                if (transformList.Count > 0)
                {
                    transforms = transformList.ToArray();
                    atlasRectangles = atlasRectangleList.ToArray();
                    colors = colorList.ToArray();

                    GL.BindBuffer(BufferTarget.ArrayBuffer, shader.GetBuffer("transform1"));
                    GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(Vector4.SizeInBytes * 4 * transforms.Length), transforms, BufferUsageHint.DynamicDraw);

                    GL.BindBuffer(BufferTarget.ArrayBuffer, shader.GetBuffer("atlasRectangle"));
                    GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(Vector4.SizeInBytes * atlasRectangles.Length), atlasRectangles, BufferUsageHint.DynamicDraw);

                    GL.BindBuffer(BufferTarget.ArrayBuffer, shader.GetBuffer("color"));
                    GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(Vector4.SizeInBytes * colors.Length), colors, BufferUsageHint.DynamicDraw);

                    GL.UniformMatrix4(shader.GetUniform("view"), false, ref FontRenderer.View);
                    GL.UniformMatrix4(shader.GetUniform("projection"), false, ref FontRenderer.Projection);

                    GL.DrawArraysInstanced(PrimitiveType.Triangles, 0, 6, transforms.Length);
                    GravityWindow.DrawCalls++;
                }
            }
            #endregion
        }

        public static void Resize(int width, int height)
        {
            GL.Viewport(0, 0, width, height);
            if(Camera.ActiveCamera != null)
                Camera.ActiveCamera.Entity.Transform.Position = Camera.ActiveCamera.Entity.Transform.Position; //Update projection matrix
            FontRenderer.Resize(width, height);
        }
    }
}
