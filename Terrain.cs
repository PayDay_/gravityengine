﻿using System;
using System.Collections.Generic;
using System.Drawing;
using OpenTK;
using static GravityEngine.Mesh;

namespace GravityEngine
{
    public class Terrain : Component
    {
        public Terrain(Entity entity, string mappath) : base(entity)
        {
            Mesh chunkMesh = Terrain.LoadMeshFromFile("heightmaps/" + mappath);

            Material terrainMaterial = new Material();
            terrainMaterial.Color = new Vector4(173f / 255f, 122f / 255f, 55f / 255f, 1f);

            MeshRenderer terrainRenderer = new MeshRenderer(entity, chunkMesh, terrainMaterial);
        }

        public Terrain(Entity entity, float[] heightdata, Size size) : base(entity)
        {
            Mesh chunkMesh = Terrain.LoadMeshFromArray(heightdata, size);

            Material terrainMaterial = new Material();
            terrainMaterial.Color = new Vector4(173f / 255f, 122f / 255f, 55f / 255f, 1f);

            MeshRenderer terrainRenderer = new MeshRenderer(entity, chunkMesh, terrainMaterial);
        }

        public static Mesh LoadMeshFromArray(float[] heightdata, Size size)
        {
            Mesh mesh;
            Vertex[] vertices;
            List<int> indices = new List<int>();
            
            vertices = new Vertex[size.Width * size.Height];
            int primitiveCount = (size.Width - 1) * (size.Height - 1) * 2;
            
            for (int y = 0; y < size.Height; y++)
            {
                for (int x = 0; x < size.Width; x++)
                {
                    int vertexID = y * size.Height + x;
                    vertices[vertexID] = new Vertex(new OpenTK.Vector3(x, heightdata[y * size.Width + x], y));
                }
            }

            for (int y = 0; y < size.Height - 1; y++)
            {
                for (int x = 0; x < size.Width- 1; x++)
                {
                    int vertexID = y * size.Height + x;
                    indices.Add(vertexID);
                    indices.Add((y + 1) * size.Height + x);
                    indices.Add(vertexID + 1);

                    indices.Add(((y + 1) * size.Height) + x);
                    indices.Add(((y + 1) * size.Height) + x + 1);
                    indices.Add(vertexID + 1);
                }
            }

            mesh = new Mesh(vertices, indices.ToArray());
            return mesh;
        }

        [Obsolete("Got moved into a standalone tool.")]
        public static Mesh LoadMeshFromFile(string path)
        {
            Mesh mesh;
            Vertex[] vertices;
            List<int> indices = new List<int>();

            Image file = Image.FromFile(path);
            vertices = new Vertex[file.Width * file.Height];
            int primitiveCount = (file.Width - 1) * (file.Height - 1) * 2;

            using (Bitmap heightmap = new Bitmap(file))
            {
                for (int y = 0; y < file.Height; y++)
                {
                    for (int x = 0; x < file.Width; x++)
                    {
                        int vertexID = y * file.Height + x;
                        vertices[vertexID] = new Vertex(new OpenTK.Vector3(x, (float)heightmap.GetPixel(x, y).R, y));
                    }
                }

                for (int y = 0; y < file.Height - 1; y++)
                {
                    for (int x = 0; x < file.Width - 1; x++)
                    {
                        int vertexID = y * file.Height + x;
                        indices.Add(vertexID);
                        indices.Add(((y + 1) * file.Height) + x);
                        indices.Add(vertexID + 1);

                        indices.Add(((y + 1) * file.Height) + x);
                        indices.Add(((y + 1) * file.Height) + x + 1);
                        indices.Add(vertexID + 1);
                    }
                }
            }

            mesh = new Mesh(vertices, indices.ToArray());
            return mesh;
        }
    }
}
