using OpenTK.Graphics.OpenGL;
using System.IO;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using OpenTK;
using System;

namespace GravityEngine
{
    public class Texture
	{
        public static List<Texture> Textures = new List<Texture>();

		internal int ID;
		public Size Size;
        public Vector4 TransparencyKey = new Vector4(256);

        public List<SpriteRenderer> Sprites = new List<SpriteRenderer>();
        public List<Material> Materials = new List<Material>();

        internal int InstanceBuffer;

        private TextureWrapMode wrapMode;
        public TextureWrapMode WrapMode
        {
            get { return wrapMode; }
            set
            {
                wrapMode = value;
                Renderer.BoundTexture = ID;
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)value);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)value);
            }
        }

        private TextureMagFilter magFilter;
        public TextureMagFilter MagFilter
        {
            get { return magFilter; }
            set
            {
                magFilter = value;
                Renderer.BoundTexture = ID;
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)magFilter);
            }
        }

        private TextureMagFilter minFilter;
        public TextureMagFilter MinFilter
        {
            get { return minFilter; }
            set
            {
                minFilter = value;
                Renderer.BoundTexture = ID;
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)minFilter);
            }
        }

        public Texture(string path, bool nearestFiltering, bool mipmapping, TextureWrapMode wrapMode, Vector4 transparencyReplacement)
		{
			ID = LoadTexture (path, transparencyReplacement, nearestFiltering, mipmapping);

            InstanceBuffer = GL.GenBuffer();
            WrapMode = wrapMode;

            Textures.Add(this);

            Console.WriteLine("Texture loaded. (Path: " + path + ")");
		}
		public Texture(string path, bool nearestFiltering, bool mipmapping) : this(path, nearestFiltering, mipmapping, TextureWrapMode.Repeat, Vector4.Zero) { }
        public Texture(string path, bool nearestFiltering) : this(path, nearestFiltering, true) {  }
        public Texture(string path) : this(path, false) { }


		private int LoadTexture(string path, Vector4 transparencyKey, bool nearestFiltering = false, bool mipmapping = true)
		{
            if (path.Length <= 0)
                return -1;

            /*if(Textures.ContainsKey(path))
            {
                return Textures[path];
            }*/

			if (!File.Exists(path))
			{
				throw new FileNotFoundException("File not found at '" + path + "'");
			}

			System.Drawing.Imaging.PixelFormat bmpFormat = System.Drawing.Imaging.PixelFormat.Format24bppRgb;
			OpenTK.Graphics.OpenGL.PixelFormat glFormat = OpenTK.Graphics.OpenGL.PixelFormat.Bgr;
            PixelInternalFormat glIntFormat = PixelInternalFormat.Rgb;
			if(path.EndsWith(".png"))
			{
				bmpFormat = System.Drawing.Imaging.PixelFormat.Format32bppArgb;
				glFormat = OpenTK.Graphics.OpenGL.PixelFormat.Bgra;
				glIntFormat = PixelInternalFormat.Rgba;
			}

			int id = GL.GenTexture();
            Renderer.BoundTexture = id;

            TransparencyKey = transparencyKey;

			Bitmap bmp = new Bitmap(path);
			//bmp.MakeTransparent(Color.FromArgb((int)Math.Round(transparencyKey.W * 255), (int)Math.Round(transparencyKey.X * 255), (int)Math.Round(transparencyKey.Y * 255), (int)Math.Round(transparencyKey.Z * 255)));
			//Console.WriteLine(Color.FromArgb((int)Math.Round(transparencyKey.W * 255), (int)Math.Round(transparencyKey.X * 255), (int)Math.Round(transparencyKey.Y * 255), (int)Math.Round(transparencyKey.Z * 255)));
			BitmapData data = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadOnly, bmpFormat);
			GL.TexImage2D(TextureTarget.Texture2D, 0, glIntFormat, data.Width, data.Height, 0, glFormat, PixelType.UnsignedByte, data.Scan0);

			Size = new Size (bmp.Width, bmp.Height);

			bmp.UnlockBits(data);

            if (nearestFiltering)
            {
                if(mipmapping)
                    GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.NearestMipmapNearest);
                else
                    GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);
            }
            else
            {
                if (mipmapping)
                    GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.LinearMipmapLinear);
                else
                    GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            }

            if(mipmapping)
                GL.GenerateMipmap(GenerateMipmapTarget.Texture2D);

			return id;
		}
	}
}

