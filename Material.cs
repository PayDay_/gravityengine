﻿using OpenTK;
using OpenTK.Graphics.OpenGL;

using System;
using System.Collections.Generic;

namespace GravityEngine
{
    public class Material
    {
        public static List<Material> Materials = new List<Material>();

        public static void UpdateMaterials()
        {
            foreach(Material material in Materials)
            {
                if (!material.vboInvalid)
                    continue;

                material.UpdateVBO();
                material.vboInvalid = false;
            }
        }

        //------------------------- INSTANCE ---------------------//

        public Vector4 Color = Vector4.One;

        private Texture textureDiffuse = null;
        public Texture TextureDiffuse { get { return textureDiffuse; } set { if (textureDiffuse != null) textureDiffuse.Materials.Remove(this); textureDiffuse = value; if (textureDiffuse != null) textureDiffuse.Materials.Add(this); } }

        public List<MeshRenderer> MeshRenderers = new List<MeshRenderer>();

        public Shader Shader;

        public readonly int VAO;
        public readonly int ElementBuffer;

        private bool vboInvalid = false;

        public Material()
        {
            Shader = new Shader("mesh.vert", "mesh.frag", true);

            VAO = GL.GenVertexArray();
            GL.BindVertexArray(VAO);

            ElementBuffer = GL.GenBuffer();

            GL.UseProgram(Shader.ProgramID);

            Shader.EnableVertexAttribArrays();

            GL.BindBuffer(BufferTarget.ArrayBuffer, Shader.GetBuffer("position"));
            GL.VertexAttribPointer(Shader.GetAttribute("position"), 3, VertexAttribPointerType.Float, false, 0, 0);

            GL.BindBuffer(BufferTarget.ArrayBuffer, Shader.GetBuffer("normal"));
            GL.VertexAttribPointer(Shader.GetAttribute("normal"), 3, VertexAttribPointerType.Float, true, 0, 0);

            GL.BindBuffer(BufferTarget.ArrayBuffer, Shader.GetBuffer("uv"));
            GL.VertexAttribPointer(Shader.GetAttribute("uv"), 2, VertexAttribPointerType.Float, true, 0, 0);

            Shader.DisableVertexAttribArrays();

            Materials.Add(this);
        }

        private void UpdateVBO()
        {
            /*List<Mesh> usedMeshes = new List<Mesh>();
            foreach(MeshRenderer renderer in MeshRenderers)
                if (!usedMeshes.Contains(renderer.mesh))
                    usedMeshes.Add(renderer.mesh);*/

            List<Mesh.Vertex> vertices = new List<Mesh.Vertex>();
            List<int> indicesList = new List<int>();

            foreach (MeshRenderer renderer in MeshRenderers)
            {
                renderer.IndexOffset = indicesList.Count;
                vertices.AddRange(renderer.Mesh.Vertices);
                indicesList.AddRange(renderer.Mesh.GetIndices(renderer.IndexOffset));
            }

            Vector3[] positions = new Vector3[vertices.Count];
            Vector3[] normals = new Vector3[vertices.Count];
            Vector2[] uvs = new Vector2[vertices.Count];
            uint[] indices = new uint[indicesList.Count];

            for (int i = 0; i < vertices.Count; i++)
            {
                positions[i] = vertices[i].Position;
                normals[i] = vertices[i].Normal;
                uvs[i] = vertices[i].UV;
            }

            for (int i = 0; i < indicesList.Count; i++)
            {
                indices[i] = (uint)indicesList[i];
            }

            GL.UseProgram(Shader.ProgramID);

            GL.BindVertexArray(VAO);
            Shader.EnableVertexAttribArrays();

            GL.BindBuffer(BufferTarget.ArrayBuffer, Shader.GetBuffer("position"));
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(positions.Length * Vector3.SizeInBytes), positions, BufferUsageHint.StaticDraw);

            GL.BindBuffer(BufferTarget.ArrayBuffer, Shader.GetBuffer("normal"));
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(normals.Length * Vector3.SizeInBytes), normals, BufferUsageHint.StaticDraw);

            GL.BindBuffer(BufferTarget.ArrayBuffer, Shader.GetBuffer("uv"));
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(uvs.Length * Vector2.SizeInBytes), uvs, BufferUsageHint.StaticDraw);

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ElementBuffer);
            GL.BufferData(BufferTarget.ElementArrayBuffer, indices.Length * sizeof(uint), indices, BufferUsageHint.StaticDraw);

            Shader.DisableVertexAttribArrays();
        }

        public void InvalidateVBO()
        {
            vboInvalid = true;
        }
    }
}
