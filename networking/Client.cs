﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace GravityEngine.Networking
{
	public static class Client
	{
		static Socket socket;
		static Queue<Packet> receivedPackets = new Queue<Packet>();
		static bool connected = false;
		static ushort id;
		static ushort ID
		{
			get { return id; }
		}
		static float lastUpdate;

		public static void Connect(string serverIP)
		{
			//Configuring the socket
			socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
			socket.Connect(serverIP, 3657);
			connected = true;

			//Listen to new packets from the server
			Thread receiverThread = new Thread(new ThreadStart(Receive));
			receiverThread.Start();

			//Get independent updates
			Thread updateThread = new Thread(new ThreadStart(Update));
			updateThread.Start();

			//Send the first message
			Console.WriteLine("Sending handshake...");
			Send(new HandshakePacket());
			Console.WriteLine("Handshake sent.");
		}

		public static void Disconnect()
		{
            if(socket != null)
    			socket.Close();
			connected = false;
		}

		public static void Send(Packet packet)
		{
			socket.Send(packet.Encode());
		}

		private static void Receive()
		{
			while (connected)
			{
				//Working with the socket
				byte[] buffer = new byte[UInt16.MaxValue];
				try
				{
					socket.Receive(buffer);
				}
				catch(SocketException)
				{
					Thread.Sleep(100);
					continue;
				}

				//Write stuff to packet-object's
				Packet received = new Packet(PacketType.Unknown);
				received.Decode(buffer);

				switch(received.Type)
				{
					case PacketType.Handshake:
						{
							throw new NotImplementedException();
						}
					case PacketType.HandshakeResponse:
						{
							HandshakeResponsePacket packet = new HandshakeResponsePacket();
							packet.Decode(buffer);
							Setup(packet);
							break;
						}
					case PacketType.Custom:
						{
							//Add the packet to the queue
							receivedPackets.Enqueue(received);
							break;
						}
				}
			}
		}

		private static void Setup(HandshakeResponsePacket packet)
		{
			if (packet.handshakeString == "GravityEngineProtocol")
			{
				id = packet.id;
				Console.WriteLine("Handshake response recieved. Client-ID is:" + ID);
			}
		}

		private static void Update()
		{
			while (connected)
			{
				lastUpdate += Time.ElapsedSeconds;
				if (lastUpdate > 1)
				{
					lastUpdate = 0;
					if (connected)
					{
						PingPacket packet = new PingPacket();
						packet.clientID = ID;
						socket.Send(packet.Encode());
					}
				}
				Thread.Sleep(50);
			}
		}
	}
}
