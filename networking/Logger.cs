﻿using System;
using System.IO;

namespace GravityEngine
{
	public static class Logger
	{
		public static void Log(String lines)
		{
			if (!Directory.Exists("logs/"))
				Directory.CreateDirectory("logs/");

			object _locker = new object();

			lock(_locker)
			{
				using (StreamWriter file = File.AppendText("logs/" + DateTime.Today.ToShortDateString().Replace('.', '_') + ".log"))
				{
					file.WriteLine(DateTime.UtcNow.ToString() + lines);

					file.Close();
				}
			}
		}
	}
}
