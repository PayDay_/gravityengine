﻿using System;
using System.Collections.Generic;

namespace GravityEngine.Networking
{
	public class Packet
	{
		public ushort clientID = 0;
		private PacketType type;
		public PacketType Type
		{
			get { return type; }
		}

		private List<byte[]> data = new List<byte[]>();
		public List<byte[]> Data
		{
			get { return data; }
		}

		public Packet(PacketType type)
		{
			this.type = type;
		}

		public void AddData(PacketType type, byte[] data)
		{
			this.data.Add(data);
			this.type = type;
		}
		public void SetData(PacketType type, List<byte[]> data)
		{
			this.data = data;
			this.type = type;
		}

		public virtual byte[] Encode()
		{
			List<byte> stream = new List<byte>();// Creating the package
			stream.Add(0); // Hold the first two bytes free
			stream.Add(0);

			#region PacketHeader
			stream.AddRange(BitConverter.GetBytes((ushort)clientID));
			stream.AddRange(BitConverter.GetBytes((ushort)type));
			#endregion

			foreach (byte[] parameter in data)
			{
				ushort lineLength = (ushort)parameter.Length; //Writing the length of the parameter
				stream.AddRange(BitConverter.GetBytes(lineLength));
				stream.AddRange(parameter);
			}
			byte[] streamArray = stream.ToArray();
			Array.Copy(BitConverter.GetBytes(streamArray.Length), 0, streamArray, 0, 2); // Writing its own length into it
			return streamArray;
		}

		public virtual void Decode(byte[] rawStream)
		{
			ushort length = BitConverter.ToUInt16(rawStream, 0);
			List<byte[]> packetData = new List<byte[]>(); //This will be filled shortly

			ushort handledByte = 2; //At which location is the untouched data?

			#region PacketHeader
			this.clientID = BitConverter.ToUInt16(rawStream, 2);
			handledByte += 2;
			this.type = (PacketType)BitConverter.ToUInt16(rawStream, 4);
			handledByte += 2;
			#endregion

			while (handledByte < length)
			{
				ushort dataLength = BitConverter.ToUInt16(rawStream, handledByte);
				handledByte += 2; //Advance the "cursor" in the package
				byte[] segmentData = new byte[dataLength];
				Array.Copy(rawStream, handledByte, segmentData, 0, dataLength);
				handledByte += dataLength; //Advance the "cursor" in the package
				packetData.Add(segmentData); //Get this stuff readable again and throw it into the List
			}
			//Fill this package
			this.data = packetData;
		}
	}

	public enum PacketType
	{
		Unknown,
		Handshake,
		HandshakeResponse,
		Custom,
		Ping
	}
}
