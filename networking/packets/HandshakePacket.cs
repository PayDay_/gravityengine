﻿using System;
using System.Text;

namespace GravityEngine.Networking
{
	internal class HandshakePacket : Packet
	{
		internal string handshakeString = "GravityEngineProtocol";
		internal ushort protocolVersion = 1;

		internal HandshakePacket() :
			base(PacketType.Handshake)
		{
		}

		public override byte[] Encode()
		{
			AddData(PacketType.Handshake, Encoding.UTF8.GetBytes(handshakeString));
			AddData(PacketType.Handshake, BitConverter.GetBytes(protocolVersion));
			return base.Encode();
		}

		public override void Decode(byte[] rawStream)
		{
			base.Decode(rawStream);
			handshakeString = Encoding.UTF8.GetString(Data[0]);
			protocolVersion = BitConverter.ToUInt16(Data[1], 0);
		}
	}
}