﻿namespace GravityEngine.Networking
{
    internal class PingPacket : Packet
	{
		internal PingPacket() :
			base(PacketType.Ping)
		{
		}
	}
}
