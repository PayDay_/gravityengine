﻿using System;
using System.Text;

namespace GravityEngine.Networking
{
	internal class HandshakeResponsePacket : Packet
	{
		internal string handshakeString = "";
		internal ushort id;

		internal HandshakeResponsePacket() :
			base(PacketType.HandshakeResponse)
		{
		}

		public override byte[] Encode()
		{
			AddData(PacketType.HandshakeResponse, Encoding.UTF8.GetBytes(handshakeString));
			AddData(PacketType.HandshakeResponse, BitConverter.GetBytes(id));
			return base.Encode();
		}

		public override void Decode(byte[] rawStream)
		{
			base.Decode(rawStream);
			handshakeString = Encoding.UTF8.GetString(Data[0]);
			id = BitConverter.ToUInt16(Data[1], 0);
		}
	}
}
