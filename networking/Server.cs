﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace GravityEngine.Networking
{
	public static class Server
	{
		public static readonly ushort MAX_CLIENTS = 32;
		public static readonly ushort TIMEOUT_SECONDS = 5;
		static Socket socket;

		static Thread receiverThread;
		static Thread updateThread;

		public static RemoteClient[] Clients = new RemoteClient[MAX_CLIENTS];
		public static bool Running { get; private set; }

		public static void Init()
		{
			SendInfoMessage(MessageType.Status, "Setting up the server...");

			socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
			socket.Bind(new IPEndPoint(IPAddress.Any, 3657));
		}

		public static void Start()
		{
			receiverThread = new Thread(new ThreadStart(Receive));
			updateThread = new Thread(new ThreadStart(Update));

			Running = true;

			receiverThread.Start();
			updateThread.Start();

			SendInfoMessage(MessageType.Status, "Server started.");
		}
		public static void Stop()
		{
			SendInfoMessage(MessageType.Status, "Shutting down...");

			Running = false;
			socket.Close();

			while (receiverThread.ThreadState != ThreadState.Stopped || updateThread.ThreadState != ThreadState.Stopped)
				Thread.Sleep(1000);
			
			SendInfoMessage(MessageType.Status, "Server stopped.");
		}

		private static RemoteClient CreateClient(EndPoint ep)
		{
			RemoteClient newClient = new RemoteClient();

			for (ushort i = 0; i < MAX_CLIENTS; i++)
			{
				if (Clients[i] == null)
				{
					Clients[i] = newClient;
					Clients[i].ID = i;
					break;
				}
			}

			//Response to the new player...
			HandshakeResponsePacket response = new HandshakeResponsePacket();
			response.handshakeString = "GravityEngineProtocol";
			response.id = newClient.ID;
			socket.SendTo(response.Encode(), ep);

			return newClient;
		}

		public static void Receive()
		{
			while (Running)
			{
				byte[] received = new byte[ushort.MaxValue];
				EndPoint sender = new IPEndPoint(IPAddress.Any, 3657);

				//Receive some data...
				try
				{
					socket.ReceiveFrom(received, ref sender);
				}
				catch(SocketException exception)
				{
					if (Running)
						throw exception;
				}

				Packet receivedPacket = new Packet(PacketType.Unknown);
				receivedPacket.Decode(received);

				switch (receivedPacket.Type)
				{
					case PacketType.Handshake:
						{
							SendInfoMessage(MessageType.Info, "New client is connected.");
							RemoteClient newClient = CreateClient(sender);
							SendInfoMessage(MessageType.Info, "New client got id " + newClient.ID + ".");
							break;
						}
					case PacketType.HandshakeResponse:
						{
							throw new NotImplementedException();
						}
					case PacketType.Ping:
						{
							socket.SendTo(receivedPacket.Encode(), sender);
							Clients[receivedPacket.clientID].TimeoutTimer = 0;
							break;
						}
				}
			}
		}

		private static void Update()
		{
			while (Running)
			{
				Time.Update();

				#region Timeout updates
				List<RemoteClient> deadClients = new List<RemoteClient>();
				foreach (RemoteClient client in Clients)
				{
					if (client == null) continue;

					if (UpdateClientTimeout(client, Time.ElapsedSeconds))
						deadClients.Add(client);
				}
				foreach (RemoteClient deletedClient in deadClients)
				{
					SendInfoMessage(MessageType.Info, "Client " + deletedClient.ID + " disconnected! (Timeout)");
					Clients[deletedClient.ID] = null;
				}
				deadClients.Clear();
				#endregion
			}
		}

		private static bool UpdateClientTimeout(RemoteClient client, float elapsedSeconds)
		{
			//Advance timer
			client.TimeoutTimer += elapsedSeconds;

			if (client.TimeoutTimer > TIMEOUT_SECONDS)
			{
				return true;
			}
			return false;
		}

		public static void ExecuteShellCommand(string command)
		{
			switch (command)
			{
				case "exit":
					{
                        Stop();
                        Environment.Exit(0);
						break;
					}
			}
		}

		public static void SendInfoMessage(MessageType type, string text)
		{
			switch (type)
			{
				case MessageType.Status: SendInfoMessage("STATUS", text); break;
				case MessageType.Info: SendInfoMessage("INFO", text); break;
				case MessageType.Debug: SendInfoMessage("DEBUG", text); break;
				case MessageType.Game: SendInfoMessage("GAME", text); break;
			}
		}

		public static void SendInfoMessage(string costumType, string text)
		{
			Console.WriteLine("[" + costumType + "] " + text);
			//Logger.Log("[" + costumType + "] " + text + "\n");
		}

		public enum MessageType
		{
			Status,
			Info,
			Debug,
			Game,
		}
	}
}
