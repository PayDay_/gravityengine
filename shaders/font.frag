#version 330 core

out vec2 TexCoords;
out vec4 TextColor;

uniform sampler2D text;
uniform bool wireframe;

void main()
{
	vec4 sampled = texture2D(text, TexCoords);    
	gl_FragColor = TextColor * sampled;

	if(wireframe)
		gl_FragColor = TextColor;
}  