﻿#version 330 core

smooth in vec2 fragTexCoord;
smooth in vec4 fragColor;

uniform sampler2D maintexture;
uniform bool textured;
uniform vec4 transparencyKey;
uniform bool wireframe;

out vec4 outputColor;

void main()
{
	if(textured)
	{
		outputColor = texture2D(maintexture, fragTexCoord);

		if(transparencyKey != vec4(256))
			if(outputColor == transparencyKey)
				outputColor = vec4(0, 0, 0, 0);
		outputColor = outputColor * fragColor;
	}
    else
	{
		outputColor = fragColor;
	}
	
	if(wireframe)
		outputColor = fragColor;
}
