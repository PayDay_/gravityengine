﻿#version 330 core

smooth in vec3 fragPos;
smooth in vec3 fragNorm;
smooth in vec2 fragUV;

uniform vec4 diffuseColor;

uniform sampler2D textureDiffuse;
uniform bool textureDiffuseUsed;

out vec4 outputColor;

void main()
{
	outputColor = diffuseColor;

	if(textureDiffuseUsed)
		outputColor *= texture2D(textureDiffuse, fragUV);
}
