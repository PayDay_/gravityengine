﻿#version 330 core

layout (location = 0) in vec2 position;
layout (location = 1) in vec2 texCoord;
layout (location = 2) in vec4 transform1;
layout (location = 3) in vec4 transform2;
layout (location = 4) in vec4 transform3;
layout (location = 5) in vec4 transform4;
layout (location = 6) in vec4 atlasRectangle;
layout (location = 7) in vec4 color;

uniform mat4 projection;
uniform mat4 view;

smooth out vec2 fragTexCoord;
smooth out vec4 fragColor;

void main()
{
	mat4 transform = mat4(transform1, transform2, transform3, transform4);
	mat4 VP = projection * view;
	vec4 pos = transform * vec4(position, 0.0, 1.0);
	gl_Position = VP * pos;

	fragTexCoord = texCoord;
	fragTexCoord.x *= atlasRectangle.z;
	fragTexCoord.x += atlasRectangle.x;
	fragTexCoord.y *= atlasRectangle.w;
	fragTexCoord.y += atlasRectangle.y;

	fragColor = color;
}
