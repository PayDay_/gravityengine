﻿#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 uv;

uniform mat4 transform;
uniform mat4 projection;
uniform mat4 view;

smooth out vec3 fragPos;
smooth out vec3 fragNorm;
smooth out vec2 fragUV;

void main()
{
	mat4 VP = projection * view;
	vec4 pos = transform * vec4(position, 1);
	gl_Position = VP * pos;

	fragPos = position;
	fragNorm = normal;
	fragUV = uv;
}
