﻿#version 330 core

in vec2 position;
in vec2 texCoord;

uniform vec4 atlasRectangle;
uniform vec4 color;

uniform mat4 transform;
uniform mat4 projection;
uniform mat4 view;

out vec2 fragTexCoord;
out vec4 fragColor;

void main()
{
	mat4 VP = projection * view;
	vec4 pos = transform * vec4(position, 0.0, 1.0);
	gl_Position = VP * pos;

	fragTexCoord = texCoord;
	fragTexCoord.x *= atlasRectangle.z;
	fragTexCoord.x += atlasRectangle.x;
	fragTexCoord.y = 1 - fragTexCoord.y;
	fragTexCoord.y *= atlasRectangle.w;
	fragTexCoord.y += atlasRectangle.y;

	fragColor = color;
}
