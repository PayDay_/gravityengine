#version 330 core

in vec4 vert;
in vec4 transform1;
in vec4 transform2;
in vec4 transform3;
in vec4 transform4;
in vec4 textColor;
in vec4 atlasRectangle;

out vec2 TexCoords;
out vec4 TextColor;

uniform mat4 projection;

void main()
{
	mat4 transform = mat4(transform1, transform2, transform3, transform4);
	
    gl_Position = projection * transform * vec4(vert.xy, 1.0, 1.0);
	
	TexCoords = vert.zw;
	TexCoords.x *= atlasRectangle.z;
	TexCoords.x += atlasRectangle.x;
	//TexCoords.y = 1 - TexCoords.y;
	TexCoords.y *= atlasRectangle.w;
	TexCoords.y += atlasRectangle.y;
	
	TextColor = textColor;
}  