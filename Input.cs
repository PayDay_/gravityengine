﻿using OpenTK;
using OpenTK.Input;
using System;
using System.Drawing;

namespace GravityEngine
{
    public static class Input
    {
        internal static void Update()
        {
            Mouse.Update();
			Keyboard.Update();
        }

        internal static void LateUpdate()
        {
            Mouse.LateUpdate();
			Keyboard.LateUpdate();
        }

        public static class Mouse
        {
            private static MouseState lastState;

            public static Point Position
            {
                get
                {
                    return new Point(OpenTK.Input.Mouse.GetState().X, OpenTK.Input.Mouse.GetState().Y);
                }

                set
                {
                    Point newPos = value;
                    if (Environment.OSVersion.Platform != PlatformID.Unix)
                    {
                        newPos = GravityWindow.Window.PointToScreen(newPos);
                    }
                    OpenTK.Input.Mouse.SetPosition(newPos.X, newPos.Y);
                }
            }

            private static Vector2 delta;
            public static Vector2 Delta
            {
                get
                {
                    return delta;
                }
            }

            internal static void OnMove(MouseMoveEventArgs e)
            {
            }

            internal static void Update()
            {
                MouseState state = OpenTK.Input.Mouse.GetState();
                delta = new Vector2(state.X - lastState.X, state.Y - lastState.Y);
            }

            internal static void LateUpdate()
            {
                lastState = OpenTK.Input.Mouse.GetState();
            }

            public static void Center()
            {
                Position = new Point(GravityWindow.Window.Width / 2, GravityWindow.Window.Height / 2);
            }

            /// <summary>
            /// Returns true if the button is started being pressed in this frame.
            /// </summary>
            /// <param name="button">The button that is checked.</param>
            /// <returns></returns>
            public static bool IsButtonPressed(MouseButton button)
            {
                bool last = lastState.IsButtonDown(button);
                bool now = OpenTK.Input.Mouse.GetState().IsButtonDown(button);

                if (!lastState.IsButtonDown(button) && OpenTK.Input.Mouse.GetState().IsButtonDown(button))
                    return true;

                return false;
            }
            /// <summary>
            /// Gets a bool indicating whether this button is pressed down.
            /// </summary>
            /// <param name="button">The button that is checked.</param>
            /// <returns></returns>
            public static bool IsButtonDown(MouseButton button)
            {
				return OpenTK.Input.Mouse.GetState().IsButtonDown(button);
            }
            /// <summary>
            /// Returns true if the button is stopped being pressed in this frame.
            /// </summary>
            /// <param name="button">The button that is checked.</param>
            /// <returns></returns>
            public static bool IsButtonReleased(MouseButton button)
            {
                if (lastState.IsButtonDown(button) && !OpenTK.Input.Mouse.GetState().IsButtonDown(button))
                    return true;

                return false;
            }
        }

        public static class Keyboard
        {
			private static KeyboardState lastState;

			internal static void Update()
			{
			}

			internal static void LateUpdate()
			{
				lastState = OpenTK.Input.Keyboard.GetState();
			}

            /// <summary>
            /// Gets a bool indicating whether this key is down.
            /// </summary>
            /// <param name="key">The key that is checked.</param>
            /// <returns></returns>
            public static bool IsKeyDown(Key key)
            {
                return OpenTK.Input.Keyboard.GetState().IsKeyDown(key);
            }
            /// <summary>
            /// Gets a bool indicating whether this key is up.
            /// </summary>
            /// <param name="key">The key that is checked.</param>
            /// <returns></returns>
            public static bool IsKeyUp(Key key)
            {
                return OpenTK.Input.Keyboard.GetState().IsKeyUp(key);
            }

			public static bool IsKeyPressed(Key key)
			{
				bool last = lastState.IsKeyDown(key);
				bool now = OpenTK.Input.Keyboard.GetState().IsKeyDown(key);

				if (!lastState.IsKeyDown(key) && OpenTK.Input.Keyboard.GetState().IsKeyDown(key))
					return true;

				return false;
			}

			public static bool IsKeyReleased(Key key)
			{
				if (lastState.IsKeyDown(key) && !OpenTK.Input.Keyboard.GetState().IsKeyDown(key))
					return true;

				return false;
			}
        }
    }
}
