﻿using OpenTK;
using System.Collections.Generic;
using System.Drawing;

namespace GravityEngine
{
    public class SlicedSprite : Component
	{
		public static List<SlicedSprite> SlicedSprites = new List<SlicedSprite>();
		private SpriteRenderer[] sprites = new SpriteRenderer[9];

        internal static bool Invalid;

		public static void Invalidate()
		{
			Invalid = true;
		}

        public override bool Enabled
        {
            get { return base.Enabled; }
            set { base.Enabled = value;  foreach (SpriteRenderer sprite in sprites) sprite.Enabled = value; }
        }

        private Vector4 color = Vector4.One;
		public Vector4 Color
        {
            get { return color; }
            set { color = value; foreach (SpriteRenderer sprite in sprites) sprite.Color = value; }
        }

		public bool Blending = false;

		private Size size = new Size();
		public Size Size
		{
			get { return size; }
			set { size = value;  Resize(); Invalidate(); }
		}

		private Vector2 pivot = Vector2.Zero;
		public Vector2 Pivot
		{
			get { return pivot; }
			set { pivot = value; Invalidate(); }
		}

		private WindowBorders borderSize = new WindowBorders();
		public WindowBorders BorderSize
		{
			get { return borderSize; }
			set { borderSize = value; Invalidate(); }
		}

		public SlicedSprite(Entity entity, Size size, Texture texture, WindowBorders borderSize) : base(entity)
		{
			SlicedSprites.Add(this);

			BorderSize = borderSize;

			sprites[0] = new SpriteRenderer(entity, new Size(borderSize.Left, borderSize.Bottom), texture, new Rectangle(0, 0, borderSize.Left, borderSize.Bottom)); 									sprites[0].Pivot = new Vector2(0, 0); 															//Bottom Left Corner
			sprites[1] = new SpriteRenderer(entity, new Size(borderSize.Left, borderSize.Top), texture, new Rectangle(0, texture.Size.Height - borderSize.Top, borderSize.Left, borderSize.Top)); 																	sprites[1].Pivot = new Vector2(0, size.Height - borderSize.Top); 								//Top Left Corner
			sprites[2] = new SpriteRenderer(entity, new Size(borderSize.Right, borderSize.Top), texture, new Rectangle(texture.Size.Width - borderSize.Right, texture.Size.Height - borderSize.Top, borderSize.Right, borderSize.Top)); 										sprites[2].Pivot = new Vector2(size.Width - borderSize.Right, size.Height - borderSize.Top);	//Top Right Corner
			sprites[3] = new SpriteRenderer(entity, new Size(borderSize.Right, borderSize.Bottom), texture, new Rectangle(texture.Size.Width - borderSize.Right, 0, borderSize.Right, borderSize.Bottom)); 	sprites[3].Pivot = new Vector2(size.Width - borderSize.Right, 0);								//Bottom Right Corner


			sprites[4] = new SpriteRenderer(entity, new Size(borderSize.Left, size.Height - borderSize.Top - borderSize.Bottom), texture, new Rectangle(0, borderSize.Bottom, borderSize.Left, texture.Size.Height - borderSize.Bottom - borderSize.Top)); 									sprites[4].Pivot = new Vector2(0, borderSize.Bottom);                             				//Left Border
			sprites[5] = new SpriteRenderer(entity, new Size(size.Width - borderSize.Left - borderSize.Right, borderSize.Top), texture, new Rectangle(borderSize.Left, texture.Size.Height - borderSize.Top, texture.Size.Width - borderSize.Left - borderSize.Right, borderSize.Top)); 												sprites[5].Pivot = new Vector2(borderSize.Left, size.Height - borderSize.Top);                  //Top Border
			sprites[6] = new SpriteRenderer(entity, new Size(borderSize.Right, size.Height - borderSize.Top - borderSize.Bottom), texture, new Rectangle(texture.Size.Width - borderSize.Right, borderSize.Bottom, borderSize.Right, texture.Size.Height - borderSize.Top - borderSize.Bottom)); sprites[6].Pivot = new Vector2(size.Width - borderSize.Right, borderSize.Bottom); 				//Right Border
			sprites[7] = new SpriteRenderer(entity, new Size(size.Width - borderSize.Left - borderSize.Right, borderSize.Bottom), texture, new Rectangle(borderSize.Left, 0, texture.Size.Width - borderSize.Left - borderSize.Right, borderSize.Bottom)); 								sprites[7].Pivot = new Vector2(borderSize.Bottom, 0);                          					//Bottom Border

			sprites[8] = new SpriteRenderer(entity, new Size(size.Width - borderSize.Left - borderSize.Right, size.Height - borderSize.Top - borderSize.Bottom), texture, new Rectangle(borderSize.Left, borderSize.Bottom, texture.Size.Width - borderSize.Left - borderSize.Right, texture.Size.Height - borderSize.Bottom - borderSize.Top)); sprites[8].Pivot = new Vector2(borderSize.Left, borderSize.Bottom); //Filling

			this.size = size;
        }
		public override void Destroy()
		{
			base.Destroy();

			SlicedSprites.Remove(this);
		}

		private void Resize()
		{
			sprites[0].Size = new Size(borderSize.Left, borderSize.Bottom); 	sprites[0].Pivot = new Vector2(0, 0);                                                           //Bottom Left Corner
			sprites[1].Size = new Size(borderSize.Left, borderSize.Top); 		sprites[1].Pivot = new Vector2(0, size.Height - borderSize.Top);                                //Top Left Corner
			sprites[2].Size = new Size(borderSize.Right, borderSize.Top); 		sprites[2].Pivot = new Vector2(size.Width - borderSize.Right, size.Height - borderSize.Top);    //Top Right Corner
			sprites[3].Size = new Size(borderSize.Right, borderSize.Bottom); 	sprites[3].Pivot = new Vector2(size.Width - borderSize.Right, 0);                               //Bottom Right Corner


			sprites[4].Size = new Size(borderSize.Left, size.Height - borderSize.Top - borderSize.Bottom); 	sprites[4].Pivot = new Vector2(0, borderSize.Bottom);                                            //Left Border
			sprites[5].Size = new Size(size.Width - borderSize.Left - borderSize.Right, borderSize.Top); 	sprites[5].Pivot = new Vector2(borderSize.Left, size.Height - borderSize.Top);                  //Top Border
			sprites[6].Size = new Size(borderSize.Right, size.Height - borderSize.Top - borderSize.Bottom); sprites[6].Pivot = new Vector2(size.Width - borderSize.Right, borderSize.Bottom);               //Right Border
			sprites[7].Size = new Size(size.Width - borderSize.Left - borderSize.Right, borderSize.Bottom); sprites[7].Pivot = new Vector2(borderSize.Bottom, 0);                                           //Bottom Border

			sprites[8].Size = new Size(size.Width - borderSize.Left - borderSize.Right, size.Height - borderSize.Top - borderSize.Bottom); sprites[8].Pivot = new Vector2(borderSize.Left, borderSize.Bottom); //Filling


            sprites[0].Size = new Size(borderSize.Left, borderSize.Bottom);     sprites[0].Pivot = new Vector2(0, 0);                                                          //Bottom Left Corner
            sprites[1].Size = new Size(borderSize.Left, borderSize.Top);             sprites[1].Pivot = new Vector2(0, size.Height - borderSize.Top);                              //Top Left Corner
            sprites[2].Size = new Size(borderSize.Right, borderSize.Top);            sprites[2].Pivot = new Vector2(size.Width - borderSize.Right, size.Height - borderSize.Top);    //Top Right Corner
            sprites[3].Size = new Size(borderSize.Right, borderSize.Bottom);         sprites[3].Pivot = new Vector2(size.Width - borderSize.Right, 0);                                //Bottom Right Corner


            sprites[4].Size = new Size(borderSize.Left, size.Height - borderSize.Top - borderSize.Bottom);          sprites[4].Pivot = new Vector2(0, borderSize.Bottom);                                            //Left Border
            sprites[5].Size = new Size(size.Width - borderSize.Left - borderSize.Right, borderSize.Top);            sprites[5].Pivot = new Vector2(borderSize.Left, size.Height - borderSize.Top);                  //Top Border
            sprites[6].Size = new Size(borderSize.Right, size.Height - borderSize.Top - borderSize.Bottom);         sprites[6].Pivot = new Vector2(size.Width - borderSize.Right, borderSize.Bottom);              //Right Border
            sprites[7].Size = new Size(size.Width - borderSize.Left - borderSize.Right, borderSize.Bottom);         sprites[7].Pivot = new Vector2(borderSize.Bottom, 0);                                            //Bottom Border

            sprites[8].Size = new Size(size.Width - borderSize.Left - borderSize.Right, size.Height - borderSize.Top - borderSize.Bottom);   sprites[8].Pivot = new Vector2(borderSize.Left, borderSize.Bottom); //Filling
        }
	}

	public struct WindowBorders
	{
		public int Top; 
		public int Right; 
		public int Bottom; 
		public int Left;

		public WindowBorders(int Top, int Right, int Bottom, int Left)
		{
			this.Left = Left;
			this.Top = Top;
			this.Right = Right;
			this.Bottom = Bottom;
		}
	}
}
