﻿using OpenTK;
using System.Collections.Generic;

namespace GravityEngine
{
    public class Rigidbody : Component
    {
        private static List<Rigidbody> Rigidbodies = new List<Rigidbody>();
		private const float GRAVITY_CONSTANT = 50;

		/// <summary>
		/// Rotational drag. Ranging from 0 to 1.
		/// Gets multiplied by the AngularVelocity.
		/// </summary>
		public float RotationalDrag = 1.0f;
		/// <summary>
		/// The mass.
		/// </summary>
        public float Mass;
		/// <summary>
		/// Ranging from 0 to 1.
		/// Gets multiplied by the Velocity.
		/// </summary>
		public float Drag = 1.0f;
		public Vector3 AngularVelocity;
		public bool FixedAngularVelocity;
        public Vector3 Velocity;
		public bool FixedVelocity;

        public Rigidbody(Entity entity, float mass) : base(entity)
        {
            Mass = mass;

            Rigidbodies.Add(this);
        }

        protected override void Update()
        {
            base.Update();

            Entity.Transform.Position += Velocity * Time.ElapsedSeconds; //Update position before velocity
			Entity.Transform.Rotation += AngularVelocity * Time.ElapsedSeconds;

			#region Drag
			AngularVelocity *= RotationalDrag; //Need work
			Velocity *= Drag;
			#endregion

			foreach (Rigidbody other in Rigidbodies)
			{
				if (other == this)
					continue;

				#region Gravity
				Vector3 direction = other.Entity.Transform.GlobalPosition - Entity.Transform.GlobalPosition;
				float distanceSquared = direction.LengthSquared;
				direction = direction.Normalized();
				float force = GRAVITY_CONSTANT * ((Mass * other.Mass) / distanceSquared);
				float acceleration = force / Mass;

				if (!FixedVelocity)
					Velocity += direction * acceleration;
				#endregion
			}
        }

        public override void Destroy()
        {
            base.Destroy();

            Rigidbodies.Remove(this);
        }
    }
}
