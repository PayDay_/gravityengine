﻿using OpenTK;
using System;
using System.Collections.Generic;

namespace GravityEngine
{
    public class Transform : Component
    {
        //--------------------- STATIC ----------------------//
        public static List<Transform> Transforms = new List<Transform>();

        public static void UpdateTransforms()
        {
            foreach (Transform tf in Transforms)
            {
                UpdateTransform(tf);
            }
        }

        protected static void UpdateTransform(Transform tf)
        {
            if (!tf.updateMatrix)
                return;

            tf.globalPosition = tf.Position;
			if (tf.Parent != null)
			{
				UpdateTransform(tf.Parent);
				tf.globalPosition += tf.Parent.GlobalPosition;
			}

			tf.Matrix = Matrix4.CreateScale(tf.Scale);

			//tf.Matrix *= Matrix4.CreateTranslation(new Vector3(-tf.Scale.X / 2, -tf.Scale.Y / 2, -tf.Scale.Z / 2));
            tf.Matrix *= Matrix4.CreateFromQuaternion(Quaternion.FromEulerAngles(tf.Rotation));
			//tf.Matrix *= Matrix4.CreateTranslation(new Vector3(tf.Scale.X / 2, tf.Scale.Y / 2, tf.Scale.Z / 2));

			tf.Matrix *= Matrix4.CreateTranslation(tf.position);

			if(tf.Parent != null)//If there is a parent...
			{
				tf.Matrix *= tf.parent.Matrix; // ... apply its matrix to ours.
			}

            tf.updateMatrix = false;

            tf.Updated?.Invoke(tf, EventArgs.Empty);
        }

        //--------------------- INSTANCE ----------------------//
        public Matrix4 Matrix = Matrix4.Identity;

        protected Vector3 position;
        public virtual Vector3 Position
        {
            set { position = value; Invalidate(); }
            get { return position; }
        }
        private Vector3 rotation;
        public virtual Vector3 Rotation
        {
            set { rotation = value; Invalidate(); }
            get { return rotation; }
        }
        private Vector3 scale;
        public virtual Vector3 Scale
        {
            set { scale = value; Invalidate(); }
            get { return scale; }
        }

        private Transform parent;
        public virtual Transform Parent
        {
            get { return parent; }
            set
            {
                if (parent != null)
                    parent.Children.Remove(this);

                parent = value;

                if (parent != null)
                {
                    parent.Children.Add(this);
                    Space = parent.Space;
                }

                Invalidate();
            }
        }
        public List<Transform> Children = new List<Transform>();

        protected Vector3 globalPosition;
        public Vector3 GlobalPosition { get { return globalPosition; } }

        new public bool Enabled { get; }

        protected bool updateMatrix;
        internal event EventHandler Updated;

        private Space space = Space.World;
        public Space Space
        {
            get { return space; }
            set { space = value; Invalidate(); foreach (Transform child in Children) child.Space = value; }
        }

        public Transform(Entity entity, Vector3 position, Vector3 rotation, Vector3 scale, Transform parent = null) : base(entity)
        {
            Position = position;
            Rotation = rotation;
            Scale = scale;
            Parent = parent;

            Transforms.Add(this);
        }

        protected void Invalidate()
        {
            updateMatrix = true;
            foreach (Transform child in Children)
                child.Invalidate();
        }

        public override void Destroy()
        {
            base.Destroy();

            foreach (Transform child in Children)
                child.Parent = Parent;
            Transforms.Remove(this);
        }
    }

    public enum Space
    {
        World,
        Screen,
        Self, //TODO: Make Self do something
    }
}
