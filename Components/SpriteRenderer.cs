﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace GravityEngine
{
    public class SpriteRenderer : Component
    {
        //const float ANTI_BLEEDING = 0.0005f;
        const float ANTI_BLEEDING = 0f;

        public static List<SpriteRenderer> SpriteRenderers = new List<SpriteRenderer>();
        internal static bool Invalid;

        public static void Invalidate()
        {
            Invalid = true;
        }

        public Vector4 Color = Vector4.One;

        private Texture texture = null;
        public Texture Texture { get { return texture; } set { if (texture != null) texture.Sprites.Remove(this); texture = value; if(texture != null) texture.Sprites.Add(this); } }

		public bool Blending = false;

		private Size size = new Size();
        public Size Size
        {
            get { return size; }
            set { size = value; Invalidate(); }
        }

        private Vector2 pivot = Vector2.Zero;
        public Vector2 Pivot
        {
            get { return pivot; }
            set { pivot = value; Invalidate(); }
        }

        private Rectangle atlasRectangle;
		public Rectangle AtlasRectangle 
		{ get { return atlasRectangle; } 
			set 
			{ 
				atlasRectangle = value;
				AtlasRectangleTextureCoordinates = new Vector4 ((float)value.X / Texture.Size.Width + ANTI_BLEEDING, (float)value.Y / Texture.Size.Height + ANTI_BLEEDING, (float)value.Width / Texture.Size.Width - ANTI_BLEEDING * 2, (float)value.Height / Texture.Size.Height - ANTI_BLEEDING * 2);
			} 
		}
		internal Vector4 AtlasRectangleTextureCoordinates;
        internal Matrix4 ModelMatrix;

		public SpriteRenderer(Entity entity, Size size, Texture texture, Rectangle atlasRectangle) : base(entity)
        {
            SpriteRenderers.Add(this);

            Size = size;
            Texture = texture;

			if(atlasRectangle == Rectangle.Empty)
				atlasRectangle = new Rectangle(0, 0, Texture.Size.Width, Texture.Size.Height);
			AtlasRectangle = atlasRectangle;

            entity.Transform.Updated += new EventHandler(TransformUpdated);
        }
		public SpriteRenderer(Entity entity, Size size, Texture texture) : this(entity, size, texture, Rectangle.Empty)
		{
		}

        public SpriteRenderer(Entity entity) : this(entity, Size.Empty, null)
        {

        }

        private void TransformUpdated(object sender, EventArgs e)
        {
            ModelMatrix = Matrix4.CreateScale(Size.Width, Size.Height, 1) * Matrix4.CreateTranslation(new Vector3(Pivot)) * Entity.Transform.Matrix;
        }

        public override void Destroy()
        {
            base.Destroy();

            Texture.Sprites.Remove(this);
            SpriteRenderers.Remove(this);
        }
    }

	public enum SpritePivot
	{
		BOTTOM_LEFT,
		CENTER,
	}
}
