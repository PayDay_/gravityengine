﻿using System.Collections.Generic;

namespace GravityEngine
{
    public abstract class Collider : Component
	{
		public Collider(Entity entity) :
			base(entity)
		{
		}

		public virtual Collider[] CheckCollisions(Collider[] collider)
		{
			List<Collider> collisions = new List<Collider>();
			foreach (Collider toCheck in collider) //Loop through all colliders... (ToDo: Don't check colliders from other loops)
			{
				if (CheckCollisionWithCollider(toCheck)) // ... check for collisions ...
					collisions.Add(toCheck); // ... and if that's true, add them to our list.
			}
			return collisions.ToArray();
		}

		public abstract bool CheckCollisionWithCollider(Collider collider);
	}
}
