﻿using System;
namespace GravityEngine
{
	public class CircleCollider : Collider
	{
		public float Radius;

		public CircleCollider(Entity entity, float radius)
			: base(entity)
		{
			this.Radius = radius;
		}

		public override bool CheckCollisionWithCollider(Collider collider)
		{
			if (collider is CircleCollider) // Same
			{
				CircleCollider otherCollider = (CircleCollider)collider;

				//Maths incoming...
				float minimalDistance = otherCollider.Radius + this.Radius;
				//Compare!
				if (minimalDistance < (otherCollider.Entity.Transform.GlobalPosition - this.Entity.Transform.GlobalPosition).Length)
				{
					return false;
				}
				else
				{
					return true;
				}
			}
			else
			{
				throw new NotImplementedException("Unknown collider!");
			}
		}
	}
}
