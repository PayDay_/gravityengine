﻿using OpenTK;
using System;
using System.Collections.Generic;

using OpenTK.Graphics.OpenGL;

namespace GravityEngine
{
    public class MeshRenderer : Component
    {
        public static List<MeshRenderer> MeshRenderers = new List<MeshRenderer>();
        internal static bool Invalid;

        public static int RenderedVertices { get; private set; }

        internal static void Init()
        {
            //Shader = new Shader("mesh.vert", "mesh.frag", true);
        }

        internal static void Render()
        {
            RenderedVertices = 0;
            GL.Disable(EnableCap.Blend);

            foreach (Material material in Material.Materials)
            {
                if (material.MeshRenderers.Count == 0)
                    continue;

                GL.BindVertexArray(material.VAO);
                GL.UseProgram(material.Shader.ProgramID);
                material.Shader.EnableVertexAttribArrays();

                GL.UniformMatrix4(material.Shader.GetUniform("view"), false, ref Renderer.View);
                GL.UniformMatrix4(material.Shader.GetUniform("projection"), false, ref Renderer.Projection);

                GL.Uniform4(material.Shader.GetUniform("diffuseColor"), ref material.Color);

                if (material.TextureDiffuse != null)
                {
                    Renderer.BoundTexture = material.TextureDiffuse.ID;
                    GL.Uniform1(material.Shader.GetUniform("textureDiffuseUsed"), 1);
                }
                else
                {
                    Renderer.BoundTexture = -1;
                    GL.Uniform1(material.Shader.GetUniform("textureDiffuseUsed"), 0);
                }

                GL.Uniform1(material.Shader.GetUniform("textureDiffuse"), 0);

                foreach (MeshRenderer renderer in material.MeshRenderers)
                {
                    if (!renderer.Enabled)
                        continue;
                    if (renderer.Mesh == null)
                        continue;

                    GL.UniformMatrix4(material.Shader.GetUniform("transform"), false, ref renderer.ModelMatrix);

                    //GL.DrawArrays(PrimitiveType.Triangles, 0, renderer.mesh.Vertices.Length);
                    GL.DrawElements(PrimitiveType.Triangles, renderer.mesh.Indices.Length, DrawElementsType.UnsignedInt, renderer.IndexOffset * sizeof(uint));

                    RenderedVertices += renderer.mesh.Indices.Length;
                    GravityWindow.DrawCalls++;
                }

                material.Shader.DisableVertexAttribArrays();
            }
        }

        internal static void Invalidate()
        {
            Invalid = true;
        }

        private Mesh mesh = null;
        public Mesh Mesh { get { return mesh; } set { if (mesh != null) mesh.Renderers.Remove(this); mesh = value; if (mesh != null) mesh.Renderers.Add(this); } }

        internal Matrix4 ModelMatrix;

        internal int IndexOffset;

        private Material material = null;
        public Material Material
        { 
            get { return material; } 
            set
            { 
                if (material != null) 
                { 
                    material.MeshRenderers.Remove(this); 
                    material.InvalidateVBO(); 
                } 
                material = value;
                if (material != null)
                {
                    material.MeshRenderers.Add(this);
                    material.InvalidateVBO();
                }
            } 
        }

        public MeshRenderer(Entity entity, Mesh mesh, Material material) : base(entity)
        {
            Mesh = mesh;
            Material = material;

            MeshRenderers.Add(this);

            entity.Transform.Updated += TransformUpdated;
        }

        public MeshRenderer(Entity entity) : this(entity, null, null) { }

        private void TransformUpdated(object sender, EventArgs e)
        {
            ModelMatrix = Entity.Transform.Matrix;
        }

        public override void Destroy()
        {
            base.Destroy();

            Mesh.Renderers.Remove(this);
            MeshRenderers.Remove(this);
        }
    }
}
