﻿using System.Collections.Generic;
using System.Drawing;
using OpenTK.Graphics.OpenGL;
using OpenTK;
using System;

namespace GravityEngine
{
    public class Camera : Component
    {
        public static List<Camera> Cameras = new List<Camera>();
        private static Camera activeCamera;
        public static Camera ActiveCamera
        {
            get { return activeCamera; }
            set
            {
                activeCamera = value;

                if (value == null)
                    return;

                GL.ClearColor(value.ClearColor);
            }
        }

        private Color clearColor = Color.CornflowerBlue;
        public Color ClearColor { get { return clearColor; } set { clearColor = value; if (ActiveCamera == this) GL.ClearColor(value); } }

        private bool orthographic = false;
        public bool Orthographic { get { return orthographic; } set { orthographic = value; UpdateProjectionMatrix(); } }

        private float fieldOfView = 1.22173f; //70 Degrees
        public float FieldOfView { get { return fieldOfView; } set { fieldOfView = value; UpdateProjectionMatrix(); } }


        private float clipNear = 0.1f;
        public float ClipNear { get { return clipNear; } set { clipNear = value; UpdateProjectionMatrix(); } }
        private float clipFar = 100f;
        public float ClipFar { get { return clipFar; } set { clipFar = value; UpdateProjectionMatrix(); } }

        private float orthographicSize = 1;
        public float OrthographicSize { get { return orthographicSize; } set { orthographicSize = value; UpdateProjectionMatrix(); } }

        internal Matrix4 ViewMatrix;
        internal Matrix4 ProjectionMatrix;

        public Camera(Entity entity) : base(entity)
        {
            Cameras.Add(this);

            entity.Transform.Updated += new EventHandler(TransformUpdated);

            if (ActiveCamera == null)
                ActiveCamera = this;
        }

        private void TransformUpdated(object sender, EventArgs e)
        {
            ViewMatrix = Entity.Transform.Matrix.ClearScale();
            UpdateProjectionMatrix();
        }

        private void UpdateProjectionMatrix()
        {
            int width = GravityWindow.Window.ClientSize.Width;
            int height = GravityWindow.Window.ClientSize.Height;

            if (width % 2 == 1)
                width--;
            if (height % 2 == 1)
                height--;

            if (Orthographic)
                ProjectionMatrix = Matrix4.CreateOrthographic((float)Math.Round((float)width * OrthographicSize), (float)Math.Round((float)height * OrthographicSize), 5000, -5000);
            else
            {
                float aspect = (float)width / (float)height;
                ProjectionMatrix = Matrix4.CreatePerspectiveFieldOfView(FieldOfView, aspect, ClipNear, ClipFar);
            }
        }

        public override void Destroy()
        {
            base.Destroy();

            Cameras.Remove(this);
        }
    }
}
