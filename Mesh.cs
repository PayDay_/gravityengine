﻿using System.Collections.Generic;

using OpenTK;

namespace GravityEngine
{
    public class Mesh
    {
        public static List<Mesh> Meshes = new List<Mesh>();

        public List<MeshRenderer> Renderers = new List<MeshRenderer>();

        public Vertex[] Vertices;
        public int[] Indices;

        public Mesh(Vertex[] vertices, int[] indices)
        {
            Vertices = vertices;
            Indices = indices;

            //RecalculateNormals();

            Meshes.Add(this);
        }

        public int[] GetIndices(int offset = 0)
        {
            if (offset > 0)
            {
                int[] indices = new int[Indices.Length];
                for (int i = 0; i < Indices.Length; i++)
                    indices[i] = Indices[i] + offset;
                return indices;
            }
            else
                return Indices;
        }

        private void RecalculateNormals()
        {
            // See if we have anything to do.
            if (Vertices.Length == 0 || Indices.Length == 0)
                return;

            Vector3[] normals = new Vector3[Vertices.Length];
            for (int i = 0; i < Indices.Length; i += 3)
            {
                int ind1 = Indices[i + 0];
                int ind2 = Indices[i + 1];
                int ind3 = Indices[i + 2];
                Vector3 v1 = Vertices[ind1].Position;
                Vector3 v2 = Vertices[ind2].Position;
                Vector3 v3 = Vertices[ind3].Position;
                Vector3 v4 = Vector3.Cross(v2 - v1, v3 - v1);
                normals[ind1] += v4;
                normals[ind2] += v4;
                normals[ind3] += v4;
            }
            for (int i = 0; i < normals.Length; ++i)
            {
                normals[i].Normalize();
                Vertices[i].Normal = normals[i];
            }
        }

        public class Vertex
        {
            public Vector3 Position = Vector3.Zero;
            public Vector3 Normal = Vector3.Zero;
            public Vector2 UV = Vector2.Zero;

            public Vertex(Vector3 position) : this(position, Vector3.Zero, Vector2.Zero) { }

            public Vertex(Vector3 position, Vector3 normal, Vector2 uv)
            {
                Position = position;
                Normal = normal;
                UV = uv;
            }

            public Vertex(Vertex vertex)
            {
                this.Position = vertex.Position;
                this.Normal = vertex.Normal;
                this.UV = vertex.UV;
            }

            public override bool Equals(object obj)
            {
                Vertex other = (Vertex)obj;
                if (other != null)
                {
                    if (this.Position.Equals(other.Position))
                        if (this.Normal.Equals(other.Normal))
                            if (this.UV.Equals(other.UV))
                                return true;

                    return false;
                }
                else
                    return base.Equals(obj);
            }

            public override int GetHashCode()
            {
                return this.Position.GetHashCode() + this.Normal.GetHashCode() + this.UV.GetHashCode();
            }
        }
    }
}
