﻿using OpenTK;
using System.Collections.Generic;
using System.Drawing;

namespace GravityEngine
{
    public class Label : Component
    {
        public static List<Label> Labels = new List<Label>();

        public string Text;
        public Font Font;
        public int FontSize;
        public StringAlignment Alignment;
        public Vector4 Color;

        #region Constructors
        public Label(Entity entity, string text, Font font, int fontSize, StringAlignment alignment, Vector4 color) : base(entity)
        {
            Text = text;
            Font = font;
            FontSize = fontSize;
            Alignment = alignment;
            Color = color;

            Labels.Add(this);
        }
        public Label(Entity entity, string text, Font font, int fontSize, StringAlignment alignment) : this(entity, text, font, fontSize, alignment, Vector4.One)
        {
        }
        public Label(Entity entity, string text, Font font, int fontSize) : this(entity, text, font, fontSize, StringAlignment.Near)
        {
        }
        public Label(Entity entity, string text, Font font) : this(entity, text, font, 20)
        {
        }
        #endregion
    }
}
