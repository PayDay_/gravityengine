﻿using System.Drawing;
using System.Drawing.Imaging;
using System.Collections.Generic;
using SharpFont;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using System.IO;
using System;

namespace GravityEngine
{
    public class Font
    {
        public static List<Font> Fonts = new List<Font>();

        public int Texture;
        public Dictionary<char, Character> Characters = new Dictionary<char, Character>();
        public int RenderedSize;

        public Font(string ttfPath, int size)
        {
            if(!File.Exists(ttfPath))
            {
                throw new FileNotFoundException(ttfPath);
            }

            RenderedSize = size;

			//If this throws an error, create a symlink to freetype named as 'libfreetype6.dll.so' (of course only on *nix)
            Library lib = new Library();

            Face face = new Face(lib, ttfPath, 0);
            face.SetPixelSizes(0, (uint)size);

            Bitmap atlas = new Bitmap(4096 * 2, 128);
            int xPixel = 0;
            for (uint c = 0; c < 256; c++)
            {
                // Load character glyph
                face.LoadChar(c, LoadFlags.Default, LoadTarget.Normal);
                face.Glyph.RenderGlyph(RenderMode.Normal);

                float texLeft = (float)xPixel / atlas.Width;
                float texBottom = 1.0f - (float)face.Glyph.Bitmap.Rows / atlas.Height;
                float texWidth = (float)face.Glyph.Bitmap.Width / atlas.Width;
                float texHeight = (float)face.Glyph.Bitmap.Rows / atlas.Height;

                Characters.Add((char)c, new Character(new Vector4(texLeft, texBottom, texWidth, texHeight), new Vector2(face.Glyph.Bitmap.Width, face.Glyph.Bitmap.Rows), new Vector2(face.Glyph.BitmapLeft, face.Glyph.BitmapTop), face.Glyph.Advance.X.Value));

                if (face.Glyph.Bitmap.Width == 0 || face.Glyph.Bitmap.Rows == 0)
                    continue;

                byte[] buffer = face.Glyph.Bitmap.BufferData;

                for (int y = 0; y < face.Glyph.Bitmap.Rows; y++)
                    for (int x = 0; x < face.Glyph.Bitmap.Width; x++)
                    {
                        byte pixel = buffer[(x + y * face.Glyph.Bitmap.Width)];
                        atlas.SetPixel(xPixel + x, y, Color.FromArgb(pixel, pixel, pixel, pixel));
                    }

                xPixel += face.Glyph.Bitmap.Width;
                xPixel += 2;
            }

            atlas.Save("atlas.png");
            atlas.RotateFlip(RotateFlipType.RotateNoneFlipY);
            BitmapData data = atlas.LockBits(new Rectangle(0, 0, atlas.Width, atlas.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            // Generate texture
            GL.GenTextures(1, out Texture);
            GL.BindTexture(TextureTarget.Texture2D, Texture);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, atlas.Width, atlas.Height, 0, OpenTK.Graphics.OpenGL.PixelFormat.Rgba, PixelType.UnsignedByte, data.Scan0);

            GL.GenerateMipmap(GenerateMipmapTarget.Texture2D);

            // Set texture options
            GL.TexParameter(TextureTarget.Texture2D, (TextureParameterName)All.TextureMaxAnisotropyExt, Renderer.MaxAnisotropySupported);

            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToBorder);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToBorder);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            
            //GL.PixelStore(PixelStoreParameter.UnpackAlignment, 1); // Disable byte-alignment restriction

            atlas.UnlockBits(data);

            lib.Dispose();
            face.Dispose();

            Fonts.Add(this);
        }

        public class Character
        {
            public Vector4 TexCoords;
            public Vector2 Size;
            public Vector2 Bearing;
            public int Advance;

            public Character(Vector4 texCoords, Vector2 size, Vector2 bearing, int advance)
            {
                TexCoords = texCoords;
                Size = size;
                Bearing = bearing;
                Advance = advance;
            }
        }
    }
}