﻿using System;
using System.Drawing;
using System.Collections.Generic;

namespace GravityEngine
{
    public class Button : Component
    {
        //--------------------------------- STATIC --------------------------------//
        public static List<Button> Buttons = new List<Button>();

        internal static void OnMouseMove(int mouseX, int mouseY)
        {
            foreach(Button button in Buttons)
            {
                bool mouseNowInside = false;
                if (mouseX >= button.Entity.Transform.GlobalPosition.X && mouseX < button.Entity.Transform.GlobalPosition.X + button.Size.Width)
                    if (mouseY >= button.Entity.Transform.GlobalPosition.Y && mouseY < button.Entity.Transform.GlobalPosition.Y + button.Size.Height)
                    {
                        mouseNowInside = true;
                    }

                if (!button.Enabled)
                    continue;

                if (!mouseNowInside)
                {
                    if(button.mouseInside)
                    {
                        button.mouseInside = false;
                        button.MouseLeave?.Invoke(null, EventArgs.Empty);
                    }
                }
                else
                {
                    if (!button.mouseInside)
                    {
                        button.mouseInside = true;
                        button.MouseEnter?.Invoke(null, EventArgs.Empty);
                    }
                }
            }
        }

        //--------------------------------- INSTANCE --------------------------------//
        private Size size = new Size();
        public Size Size
        {
            get { return size; }
            set { size = value; }
        }

        public event EventHandler MouseEnter;
        public event EventHandler MouseDown;
        public event EventHandler MouseUp;
        public event EventHandler MouseLeave;

        private bool mouseInside;
        private bool down;

        public Button(Entity entity, Size size) : base(entity)
        {
            Size = size;

            Buttons.Add(this);
        }

        protected override void Update()
        {
            base.Update();

            if (!Enabled)
                return;

            if (down)
            {
                if (Input.Mouse.IsButtonReleased(OpenTK.Input.MouseButton.Left))
                {
                    down = false;
                    MouseUp?.Invoke(null, EventArgs.Empty);
                }
            }

            if (!mouseInside)
                return;

            if (Input.Mouse.IsButtonPressed(OpenTK.Input.MouseButton.Left))
            {
                down = true;
                MouseDown?.Invoke(null, EventArgs.Empty);
            }
        }

        public override void Destroy()
        {
            base.Destroy();

            Buttons.Remove(this);
        }
    }
}
