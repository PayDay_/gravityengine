﻿using System;
using System.Collections.Generic;
using System.Drawing;
using OpenTK;
using OpenTK.Input;

namespace GravityEngine
{
	public class TextField : Component
	{
		//--------------------------------- STATIC --------------------------------//
		public static List<TextField> TextFields = new List<TextField>();

		internal static void OnMouseMove(int mouseX, int mouseY)
		{
			foreach (TextField textField in TextFields)
			{
				bool mouseNowInside = false;
				if (mouseX >= textField.Entity.Transform.GlobalPosition.X && mouseX < textField.Entity.Transform.GlobalPosition.X + textField.Size.Width)
					if (mouseY >= textField.Entity.Transform.GlobalPosition.Y && mouseY < textField.Entity.Transform.GlobalPosition.Y + textField.Size.Height)
					{
						mouseNowInside = true;
					}

				if (!mouseNowInside)
				{
					if (textField.mouseInside)
					{
						textField.mouseInside = false;
                        textField.MouseLeave?.Invoke(null, EventArgs.Empty);
                    }
				}
				else
				{
					if (!textField.mouseInside)
					{
						textField.mouseInside = true;
                        textField.MouseEnter?.Invoke(null, EventArgs.Empty);
                    }
				}
			}
		}

		internal static void OnTextInput(KeyPressEventArgs e)
		{
			foreach (TextField textField in TextFields)
			{
				textField.InputText(e);
			}
		}

		//--------------------------------- INSTANCE --------------------------------//
		private Size size = new Size();
		public Size Size
		{
			get { return size; }
			set { size = value; }
		}

		public string Text = "";

		//Events
		public event EventHandler MouseEnter;
		public event EventHandler MouseDown;
		public event EventHandler MouseUp;
		public event EventHandler MouseLeave;
		public event EventHandler Select;
		public event EventHandler Deselect;
		public event EventHandler Enter;

		//States
		private bool mouseInside = false;
		private bool selected = false;
		private float cursorUpdateTime;
		private bool visibleCursor;

		//Labels
		private Label label;
		private Entity labelTransformer;

		//Sprites
		private SlicedSprite field;

		#region Constructors
		public TextField(Entity entity, Size size, Font font, int fontSize, Texture texture, WindowBorders borders, StringAlignment alignment, Vector4 color) : base(entity)
        {
            Entity spriteTransformer = new Entity(Space.Screen, entity.Transform);
            spriteTransformer.Transform.Position = new Vector3(0, -borders.Top - borders.Bottom, 0);
			this.field = new SlicedSprite(spriteTransformer, size, texture, borders);

			this.labelTransformer = new Entity(Space.Screen, entity.Transform);
			this.labelTransformer.Transform.Position = new Vector3(borders.Left, 0, -1);
			this.label = new Label(labelTransformer, "", font, fontSize, alignment, color);

			this.MouseDown += Select;
			this.Size = size;

			TextFields.Add(this);
		}
		public TextField(Entity entity, Size size, Font font, int fontSize, Texture texture, WindowBorders borders, StringAlignment alignment) : this(entity, size, font, fontSize, texture, borders, alignment, new Vector4(1, 1, 1, 1))
        {
		}
		public TextField(Entity entity, Size size, Font font, int fontSize, Texture texture, WindowBorders borders) : this(entity, size, font, fontSize, texture, borders, StringAlignment.Near)
        {
		}
		public TextField(Entity entity, Size size, Font font, Texture texture, WindowBorders borders) : this(entity, size, font, size.Height - borders.Top - borders.Bottom, texture, borders) //TODO: Change fontsize to fit size
        {
		}
		#endregion

		protected override void Update()
		{
			base.Update();

			cursorUpdateTime += Time.ElapsedSeconds;
			if (cursorUpdateTime > 0.5f)
			{
				visibleCursor = !visibleCursor;
				cursorUpdateTime = 0;
			}

			if (selected)
			{
				if (Input.Mouse.IsButtonReleased(MouseButton.Left))
				{
                    MouseUp?.Invoke(null, EventArgs.Empty);
                }

				if (Input.Keyboard.IsKeyPressed(Key.Enter))
				{
					KeyPress(Key.Enter);
				}

				if (Input.Keyboard.IsKeyPressed(Key.BackSpace))
				{
					KeyPress(Key.BackSpace);
				}
			}

			if (Input.Mouse.IsButtonPressed(MouseButton.Left))
			{
				if (mouseInside)
				{
                    MouseDown?.Invoke(null, EventArgs.Empty);

                    if (!selected)
					{
						this.selected = true;
                        Select?.Invoke(null, EventArgs.Empty);
                    }
				}
				else
				{
					if (selected)
					{
						this.selected = false;
                        Deselect?.Invoke(null, EventArgs.Empty);
                    }
				}
			}
			if (Text != null)
				label.Text = Text;
			else
				label.Text = "";
			
			if (visibleCursor && selected)
				label.Text += "|";
		}

		private void KeyPress(Key key)
		{
			switch (key)
			{
				case Key.Enter:
                    Enter?.Invoke(null, EventArgs.Empty);
                    Text = "";
				break;
				case Key.BackSpace:
					if(Text.Length > 0)
						Text = Text.Remove(Text.Length - 1, 1);
					break;
			}
		}

		private void InputText(KeyPressEventArgs e)
		{
			if(selected)
				this.Text += e.KeyChar;
		}

		public override void Destroy()
		{
			base.Destroy();

			TextFields.Remove(this);
		}
	}
}
