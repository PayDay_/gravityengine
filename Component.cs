﻿using System.Collections.Generic;

namespace GravityEngine
{
    public class Component
    {
        //--------------------------------- STATIC --------------------------------//
        public static List<Component> Components = new List<Component>();

        internal static void UpdateComponents()
        {
			Component[] components = Components.ToArray();
			foreach (Component component in Components)
                component.Update();
        }

        //-------------------------------- INSTANCE -------------------------------//
        public Entity Entity;

        private bool enabled = true;
        public virtual bool Enabled
        {
            get { return enabled; }
            set { enabled = value; }
        }

        public Component(Entity entity)
        {
            Entity = entity;

            Entity.Components.Add(this);
            Components.Add(this);
        }

        protected virtual void Update() { }

        public virtual void Destroy()
        {
            Entity.Components.Remove(this);
            Entity = null;

            Components.Remove(this);
        }
    }
}
