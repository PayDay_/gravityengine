﻿using System.IO;

namespace GravityEngine
{
    public static class SettingsManager
	{
		private const string SETTINGS_FILE = "engine.cfg";

		public static System.Drawing.Size Resolution;

		public static bool Load()
		{
			if (File.Exists(SETTINGS_FILE))
			{
				string[] lines = File.ReadAllLines(SETTINGS_FILE);
				foreach (string line in lines)
				{
					string[] parameters = line.Split(' ');
					switch (parameters[0])
					{
						case "res": { Resolution.Width = int.Parse(parameters[1]); Resolution.Height = int.Parse(parameters[2]); break; }
					}
				}
				return true;
			}
			else
				return false;
		}
	}
}
