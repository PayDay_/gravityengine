﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Globalization;

using OpenTK;

namespace GravityEngine
{
    public static class Wavefront
    {
        public static Mesh Import(string path)
        {
            List<Vector3> positions = new List<Vector3>();
            List<Vector3> normals = new List<Vector3>();
            List<Vector2> uvs = new List<Vector2>();

            List<int> positionIndices = new List<int>();
            List<int> normalIndices = new List<int>();
            List<int> uvIndices = new List<int>();

            List<Mesh.Vertex> vertices = new List<Mesh.Vertex>();

            string[] lines = File.ReadAllLines(path);
            foreach(string line in lines)
            {
                string editedLine = line.Trim();
                string[] splitLine = editedLine.Split(' ');
                switch (splitLine[0])
                {

                    case "v":
                        float x = float.Parse(splitLine[1], CultureInfo.InvariantCulture);
                        float y = float.Parse(splitLine[2], CultureInfo.InvariantCulture);
                        float z = float.Parse(splitLine[3], CultureInfo.InvariantCulture);
                        positions.Add(new Vector3(x, y, z));
                        break;
                    case "vn":
                        x = float.Parse(splitLine[1], CultureInfo.InvariantCulture);
                        y = float.Parse(splitLine[2], CultureInfo.InvariantCulture);
                        z = float.Parse(splitLine[3], CultureInfo.InvariantCulture);
                        normals.Add(new Vector3(x, y, z));
                        break;
                    case "vt":
                        float u = float.Parse(splitLine[1], CultureInfo.InvariantCulture);
                        float v = float.Parse(splitLine[2], CultureInfo.InvariantCulture);
                        uvs.Add(new Vector2(u, v));
                        break;
                    case "f":
                        int slashCount = 0;
                        for (int i = 0; i < splitLine[1].Length; i++)
                            if (splitLine[1][i] == '/')
                                slashCount++;

                        if (slashCount == 0) //Only position
                        {
                            int pos = int.Parse(splitLine[1]);
                            positionIndices.Add(pos);
                            pos = int.Parse(splitLine[2]);
                            positionIndices.Add(pos);
                            pos = int.Parse(splitLine[3]);
                            positionIndices.Add(pos);
                        }
                        else if (slashCount == 1) //Position and UV
                        {
                            string[] slashSplit = splitLine[1].Split('/');
                            int pos = int.Parse(slashSplit[0]);
                            int uv = int.Parse(slashSplit[1]);
                            positionIndices.Add(pos);
                            uvIndices.Add(uv);

                            slashSplit = splitLine[2].Split('/');
                            pos = int.Parse(slashSplit[0]);
                            uv = int.Parse(slashSplit[1]);
                            positionIndices.Add(pos);
                            uvIndices.Add(uv);

                            slashSplit = splitLine[3].Split('/');
                            pos = int.Parse(slashSplit[0]);
                            uv = int.Parse(slashSplit[1]);
                            positionIndices.Add(pos);
                            uvIndices.Add(uv);
                        }
                        else if (slashCount == 2)
                        {
                            if (splitLine[1].Contains("//")) //Position and Normal
                            {
                                string[] slashSplit = splitLine[1].Split('/');
                                int pos = int.Parse(slashSplit[0]);
                                int norm = int.Parse(slashSplit[2]);
                                positionIndices.Add(pos);
                                normalIndices.Add(norm);

                                slashSplit = splitLine[2].Split('/');
                                pos = int.Parse(slashSplit[0]);
                                norm = int.Parse(slashSplit[2]);
                                positionIndices.Add(pos);
                                normalIndices.Add(norm);

                                slashSplit = splitLine[3].Split('/');
                                pos = int.Parse(slashSplit[0]);
                                norm = int.Parse(slashSplit[2]);
                                positionIndices.Add(pos);
                                normalIndices.Add(norm);
                            }
                            else //Position, UV and Normal
                            {
                                string[] slashSplit = splitLine[1].Split('/');
                                int pos = int.Parse(slashSplit[0]);
                                int uv = int.Parse(slashSplit[1]);
                                int norm = int.Parse(slashSplit[2]);
                                positionIndices.Add(pos);
                                uvIndices.Add(uv);
                                normalIndices.Add(norm);

                                slashSplit = splitLine[2].Split('/');
                                pos = int.Parse(slashSplit[0]);
                                uv = int.Parse(slashSplit[1]);
                                norm = int.Parse(slashSplit[2]);
                                positionIndices.Add(pos);
                                uvIndices.Add(uv);
                                normalIndices.Add(norm);

                                slashSplit = splitLine[3].Split('/');
                                pos = int.Parse(slashSplit[0]);
                                uv = int.Parse(slashSplit[1]);
                                norm = int.Parse(slashSplit[2]);
                                positionIndices.Add(pos);
                                uvIndices.Add(uv);
                                normalIndices.Add(norm);
                            }
                        }
                        break;
                }
            }

            Dictionary<Tuple<int, int, int>, int> sharedVertices = new Dictionary<Tuple<int, int, int>, int>();

            int[] indices = new int[positionIndices.Count];
            for(int i = 0; i < positionIndices.Count; i++)
            {
                Vector3 position = positions[positionIndices[i] - 1];

                Vector3 normal = Vector3.Zero;
                if (normalIndices.Count > 0)
                    normal = normals[normalIndices[i] - 1];

                Vector2 uv = Vector2.Zero;
                if (uvIndices.Count > 0)
                    uv = uvs[uvIndices[i] - 1];

                Mesh.Vertex newVertex = new Mesh.Vertex(position, normal, uv);
                int exists = -1;
                
                if (!sharedVertices.TryGetValue(new Tuple<int, int, int>(positionIndices[i] - 1, normalIndices[i] - 1, 0), out exists))
                {
                    indices[i] = vertices.Count;
                    sharedVertices.Add(new Tuple<int, int, int>(positionIndices[i] - 1, normalIndices[i] - 1, 0), vertices.Count);
                    vertices.Add(newVertex);
                }
                else
                    indices[i] = exists;
            }

            return new Mesh(vertices.ToArray(), indices);
        }
    }
}
