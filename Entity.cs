﻿using OpenTK;
using System.Collections.Generic;

namespace GravityEngine
{
    public class Entity
    {
        public static List<Entity> Entities = new List<Entity>();

        public Transform Transform;
        public List<Component> Components = new List<Component>();

        private bool enabled = true;
        public virtual bool Enabled
        {
            get
            {
                if (Transform.Parent != null)
                    return Transform.Parent.Entity.Enabled && enabled;
                else
                    return enabled;
            }
            set
            {
                enabled = value;
                foreach (Transform child in Transform.Children)
                {
                    child.Entity.Enabled = enabled;
                }
                foreach (Component component in Components)
                {
                    component.Enabled = enabled;
                }
            }
        }

        public Entity(Space transformSpace = Space.World, Transform parent = null) : this(Vector3.Zero, Vector3.Zero, Vector3.One, parent, transformSpace)
        {
        }
        public Entity(Vector3 position, Transform parent = null, Space transformSpace = Space.World) : this(position, Vector3.Zero, Vector3.One, parent, transformSpace)
        {
        }
        public Entity(Vector3 position, Vector3 rotation, Vector3 scale, Transform parent = null, Space transformSpace = Space.World)
        {
            Transform = new Transform(this, position, rotation, scale, parent);
            Transform.Space = transformSpace;
            Entities.Add(this);
        }

        internal static void UpdateEntities()
        {
            foreach (Entity entity in Entities)
            {
                entity.Update();
            }
        }

        protected virtual void Update()
        {

        }

        public virtual void Destroy()
        {
            Component[] components = Components.ToArray();
            foreach (Component component in components)
                component.Destroy();

            Transform = null;
            Entities.Remove(this);
        }
    }
}
