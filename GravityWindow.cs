﻿using System;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Input;

namespace GravityEngine
{
    public class GravityWindow : GameWindow
    {
        public static GravityWindow Window;
        public static int DrawCalls;

        public GravityWindow(int fsaaSamples = 0) : base(800, 600, new GraphicsMode(32, 24, 0, fsaaSamples, 0, 2, false), "Loading...", GameWindowFlags.Default, DisplayDevice.Default, 3, 3, GraphicsContextFlags.Default)
        {
            Window = this;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            Screen.Init();
			#region Settings
			if (SettingsManager.Load())
			{
				this.Title = "Gravity";
				this.ClientSize = new System.Drawing.Size(SettingsManager.Resolution.Width, SettingsManager.Resolution.Height);
			}
			else
			{
				this.Title = "Gravity(Secure Mode)";
				Console.WriteLine("No valid engine.cfg!");
			}
            #endregion

            GraphicsContext.CurrentContext.SwapInterval = 0;
            this.VSync = VSyncMode.Off;

            Renderer.Init();
        }

        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            base.OnUpdateFrame(e);

            Time.Update();

            Input.Update();
            Material.UpdateMaterials();
            Transform.UpdateTransforms();
            Entity.UpdateEntities();
            Component.UpdateComponents();

            Input.LateUpdate();
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);

            Renderer.Render();
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);

            Renderer.Resize(ClientSize.Width, ClientSize.Height);
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
        }

        protected override void OnMouseMove(MouseMoveEventArgs e)
        {
            base.OnMouseMove(e);

            int x = e.X;
            int y = ClientSize.Height - e.Y;

            Button.OnMouseMove(x, y);
			TextField.OnMouseMove(x, y);
            Input.Mouse.OnMove(e);
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);
        }

        protected override void OnKeyDown(KeyboardKeyEventArgs e)
        {
            base.OnKeyDown(e);
        }

        protected override void OnKeyPress(KeyPressEventArgs e)
		{
			base.OnKeyPress(e);
			TextField.OnTextInput(e);
		}
    }
}
