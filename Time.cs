﻿using System.Diagnostics;

namespace GravityEngine
{
    public static class Time
    {
        private static Stopwatch deltaCounter = new Stopwatch();
        public static float ElapsedSeconds;
        public static float ElapsedMilliseconds;

        private static int frameCount;
        public static float FramesPerSecond;
        private const int FRAMES_PER_UPDATE = 60;

        public static void Update()
        {
            deltaCounter.Stop();
            ElapsedSeconds = (float)deltaCounter.Elapsed.TotalSeconds;
            ElapsedMilliseconds = (float)deltaCounter.Elapsed.TotalMilliseconds;
            deltaCounter.Reset();
            deltaCounter.Start();

            if (frameCount >= FRAMES_PER_UPDATE)
            {
                FramesPerSecond = frameCount / ElapsedSeconds;
                FramesPerSecond /= FRAMES_PER_UPDATE;
                frameCount = 0;
            }
            frameCount++;
        }
    }
}
