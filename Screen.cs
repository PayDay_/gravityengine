﻿using System;

namespace GravityEngine
{
    public static class Screen
    {
        const int DPI_DEFAULT = 96;

        /*[DllImport("gdi32.dll")]
        static extern int GetDeviceCaps(IntPtr hdc, int nIndex);
        enum DeviceCap
        {
            VERTRES = 10,
            DESKTOPVERTRES = 117,

            // http://pinvoke.net/default.aspx/gdi32/GetDeviceCaps.html
        }*/

        public static float ScalingFactor;
        public static int DPI;

        internal static void Init()
        {
			//ScalingFactor = GetScalingFactor();
			ScalingFactor = 1.5f;
            DPI = (int)Math.Round(DPI_DEFAULT * ScalingFactor);
        }

        /*private static float GetScalingFactor()
        {
            Graphics g = Graphics.FromHwnd(IntPtr.Zero);
            IntPtr desktop = g.GetHdc();
            //int LogicalScreenHeight = GetDeviceCaps(desktop, (int)DeviceCap.VERTRES);
            //int PhysicalScreenHeight = GetDeviceCaps(desktop, (int)DeviceCap.DESKTOPVERTRES);

            //float ScreenScalingFactor = (float)PhysicalScreenHeight / (float)LogicalScreenHeight;

            return ScreenScalingFactor; // 1.25 = 125%
        }*/
    }
}
