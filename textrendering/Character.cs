﻿using System;

namespace GravityEngine
{
    public class Character
    { 

        //ID of character
        public Int16 ID { get; private set; }

        //x-Coordinate of character's square in fonttexture
        public Int16 X { get; private set; }

        //y-Coordinate of character's square in fonttexture
        public Int16 Y { get; private set; }

        //width of character's square in fonttexture
        public Int16 WIDTH { get; private set; }

        //height of character's square in fonttexture
        public Int16 HEIGHT { get; private set; }

        //charachter's xoffset
        public Int16 XOFFSET { get; private set; }

        //character's yoffset
        public Int16 YOFFSET { get; private set; }

        //character's xadvance
        public Int16 XADVANCE { get; private set; }

        //character's page
        public Int16 PAGE { get; private set; }

        //character's chnl
        public Int16 CHNL { get; private set; }

        public Character(short id, short x, short y, short width, short height, short xoffset, short yoffset, short xadvance, short page, short chnl)
        {
            this.ID = id;
            this.X = x;
            this.Y = y;
            this.WIDTH = width;
            this.HEIGHT = height;
            this.XOFFSET = xoffset;
            this.YOFFSET = yoffset;
            this.XADVANCE = xadvance;
            this.PAGE = page;
            this.CHNL = chnl;
        }


    }
}
