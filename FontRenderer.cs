﻿using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using System.Drawing;

namespace GravityEngine
{
    internal static class FontRenderer
    {
        private static Shader fontShader;

        private static int fontVAO;
        public static Matrix4 Projection;
        public static Matrix4 View = Matrix4.Identity;
        //public static Matrix4 View = Matrix4.CreateTranslation(0, 0, -500);

        public static void Init()
        {
            fontShader = new Shader("font.vert", "font.frag", true);

            GL.UseProgram(fontShader.ProgramID);
            CreateFontVBO();
        }

        private static void CreateFontVBO()
        {
            List<Vector4> verts = new List<Vector4>();
            verts.Add(new Vector4(1.0F, 1.0F, 1, 1));
            verts.Add(new Vector4(0.0F, 1.0F, 0, 1));
            verts.Add(new Vector4(1.0F, 0.0F, 1, 0));

            verts.Add(new Vector4(0.0F, 1.0F, 0, 1));
            verts.Add(new Vector4(0.0F, 0.0F, 0, 0));
            verts.Add(new Vector4(1.0F, 0.0F, 1, 0));

            fontVAO = GL.GenVertexArray();
            GL.BindVertexArray(fontVAO);

            fontShader.EnableVertexAttribArrays();

            GL.BindBuffer(BufferTarget.ArrayBuffer, fontShader.GetBuffer("vert"));
            GL.BufferData(BufferTarget.ArrayBuffer, new IntPtr(Vector4.SizeInBytes * verts.Count), verts.ToArray(), BufferUsageHint.StaticDraw);
            GL.VertexAttribPointer(fontShader.GetAttribute("vert"), 4, VertexAttribPointerType.Float, false, 0, 0);

            int transformAttribute = fontShader.GetAttribute("transform1");
            GL.BindBuffer(BufferTarget.ArrayBuffer, fontShader.GetBuffer("transform1"));
            GL.VertexAttribPointer(transformAttribute, 4, VertexAttribPointerType.Float, false, 4 * Vector4.SizeInBytes, (IntPtr)0);
            GL.VertexAttribPointer(transformAttribute + 1, 4, VertexAttribPointerType.Float, false, 4 * Vector4.SizeInBytes, (IntPtr)(1 * Vector4.SizeInBytes));
            GL.VertexAttribPointer(transformAttribute + 2, 4, VertexAttribPointerType.Float, false, 4 * Vector4.SizeInBytes, (IntPtr)(2 * Vector4.SizeInBytes));
            GL.VertexAttribPointer(transformAttribute + 3, 4, VertexAttribPointerType.Float, false, 4 * Vector4.SizeInBytes, (IntPtr)(3 * Vector4.SizeInBytes));
            GL.VertexAttribDivisor(transformAttribute, 1);
            GL.VertexAttribDivisor(transformAttribute + 1, 1);
            GL.VertexAttribDivisor(transformAttribute + 2, 1);
            GL.VertexAttribDivisor(transformAttribute + 3, 1);

            GL.BindBuffer(BufferTarget.ArrayBuffer, fontShader.GetBuffer("atlasRectangle"));
            GL.VertexAttribPointer(fontShader.GetAttribute("atlasRectangle"), 4, VertexAttribPointerType.Float, true, 0, 0);
            GL.VertexAttribDivisor(fontShader.GetAttribute("atlasRectangle"), 1);

            GL.BindBuffer(BufferTarget.ArrayBuffer, fontShader.GetBuffer("textColor"));
            GL.VertexAttribPointer(fontShader.GetAttribute("textColor"), 4, VertexAttribPointerType.Float, false, 0, 0);
            GL.VertexAttribDivisor(fontShader.GetAttribute("textColor"), 1);

            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);

            fontShader.DisableVertexAttribArrays();
        }

        public static void Render()
        {
            GL.BindVertexArray(fontVAO);
            GL.UseProgram(fontShader.ProgramID);
            fontShader.EnableVertexAttribArrays();

            if (Debug.Wireframe)
                GL.Uniform1(fontShader.GetUniform("wireframe"), 1);
            else
                GL.Uniform1(fontShader.GetUniform("wireframe"), 0);

            DrawLabelsInstanced();

            fontShader.DisableVertexAttribArrays();
        }

        private static void DrawLabelsInstanced()
        {
            foreach (Font font in Font.Fonts)
            {
                List<Label> labels = new List<Label>();
                foreach (Label label in Label.Labels)
                    if (label.Enabled)
                        if (label.Font == font)
                            labels.Add(label);

                if (labels.Count == 0)
                    continue;

                int charCount = 0;
                foreach (Label label in labels)
                    charCount += label.Text.Length;

                Matrix4[] matrices = new Matrix4[charCount];
                Vector4[] atlasRectangles = new Vector4[charCount];
                Vector4[] colors = new Vector4[charCount];

                int currentChar = 0;
                foreach (Label label in labels)
                {
                    float scale = (float)label.FontSize / label.Font.RenderedSize;

                    float labelWidth = 0;
                    for (int i = 0; i < label.Text.Length; i++)
                        labelWidth += (label.Font.Characters[label.Text[i]].Advance >> 6);
                    labelWidth *= scale;

                    int x = (int)label.Entity.Transform.GlobalPosition.X;
                    if (label.Alignment == StringAlignment.Center)
                        x -= (int)(labelWidth / 2);
                    else if (label.Alignment == StringAlignment.Far)
                        x -= (int)(labelWidth);

					int xLineStart = x;

                    //float y = (float)GravityWindow.Window.Height - label.Entity.Transform.GlobalPosition.Y;
                    int y = (int)(label.Entity.Transform.GlobalPosition.Y);
                    float z = label.Entity.Transform.GlobalPosition.Z;

					for (int i = 0; i < label.Text.Length; i++)
					{
						Font.Character character = label.Font.Characters[label.Text[i]];

						if (label.Text[i] == '\n')
						{
							y -= (label.FontSize + 2); //TODO: Add option for line clearance.
                            x = xLineStart;
                            continue;
						}

                        int xPos = (int)(x + character.Bearing.X * scale);
                        int yPos = (int)Math.Round(y - (character.Size.Y - character.Bearing.Y) * scale);

                        int w = (int)Math.Round(character.Size.X * scale);
                        int h = (int)Math.Round(character.Size.Y * scale);

                        Matrix4 matrix = Matrix4.CreateScale(w, h, 1);
                        matrix *= Matrix4.CreateTranslation(xPos, yPos, z);
                        matrices[currentChar] = matrix;

                        atlasRectangles[currentChar] = character.TexCoords;

                        colors[currentChar] = label.Color;

                        currentChar++;

                        // Now advance cursors for next glyph (note that advance is number of 1/64 pixels)
                        x += (int)((character.Advance >> 6) * scale); // Bitshift by 6 to get value in pixels (2^6 = 64)
                    }
                }

                GL.BindBuffer(BufferTarget.ArrayBuffer, fontShader.GetBuffer("transform1"));
                GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(Vector4.SizeInBytes * 4 * charCount), matrices, BufferUsageHint.DynamicDraw);

                GL.BindBuffer(BufferTarget.ArrayBuffer, fontShader.GetBuffer("atlasRectangle"));
                GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(Vector4.SizeInBytes * charCount), atlasRectangles, BufferUsageHint.DynamicDraw);

                GL.BindBuffer(BufferTarget.ArrayBuffer, fontShader.GetBuffer("textColor"));
                GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(Vector4.SizeInBytes * charCount), colors, BufferUsageHint.DynamicDraw);

                Renderer.BoundTexture = font.Texture;

                // Render quad
                GL.DrawArraysInstanced(PrimitiveType.Triangles, 0, 6, charCount);
                GravityWindow.DrawCalls++;
            }
        }

        public static void Resize(int width, int height)
        {
            GL.UseProgram(fontShader.ProgramID);
            Projection = Matrix4.CreateOrthographicOffCenter(0, GravityWindow.Window.Width, 0, GravityWindow.Window.Height, 500, -500);
            GL.UniformMatrix4(fontShader.GetUniform("projection"), false, ref Projection);
        }
    }
}
